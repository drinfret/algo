{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Tableaux Dynamiques\n",
    "## Réalisation de l'interface `IList<>` avec un tableau de taille variable\n",
    "- [Interface `IList<>`](https://docs.microsoft.com/en-us/dotnet/api/system.collections.ilist?view=netcore-3.1)\n",
    "- **Liste**: collection ordonnée d'objets pouvant être accédé par leur index\n",
    "- Méthodes clés:\n",
    "    - `Add`: ajouter un élément à la fin de la liste\n",
    "    - `Insert`: insérer un élément à un index donné dans la liste\n",
    "    - `Contains` et `IndexOf`: vérification de la présence d'un élément dans la liste\n",
    "    - `Remove` et `RemoveAt`: enlever un élément de la liste, soit en le recherchant, soit selon son index"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Représentation de base de la *Liste-Tableau*\n",
    "- En anglais: *ArrayList*\n",
    "- Classe générique, avec type générique pour les éléments de la liste: `ArrayList<TE>`\n",
    "- Un tableau d'éléments de type générique: `TE[] tab`\n",
    "- 2 propriétés:\n",
    "    - `Count`: le nombre d'éléments dans la liste\n",
    "    - `Capacity`: la taille du tableau `tab`\n",
    "- `Count` $\\leq$ `Capacity`\n",
    "    - il va normalement y avoir de l'espace inutilisée dans le tableau\n",
    "    - par exemple, si la capacité est 10 et que le nombre d'éléments dans la liste est 6, il y aura 4 espaces libres dans le tableau\n",
    "    - les espaces libres seront toujours à la fin, il ne peut pas y avoir d'espaces libres (ou de trous) au milieu ou au début du tableau\n",
    "        - à moins que le nombre d'éléments soit 0, et dans ce cas tous les espaces sont libres"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "![Tableau dynamique - espaces libres](../images/ArrayList_libres.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Ajout et insertion d'éléments dans une liste-tableau\n",
    "- La méthode `Add` ajoute des éléments à la fin de la liste, donc à la fin de la partie active du tableau\n",
    "\n",
    "```\n",
    "méthode Add(item):\n",
    "    tab[Count] = item\n",
    "    Count++\n",
    "```\n",
    "![Tableau dynamique - Ajouter un élément](../images/ArrayList_ajouter.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "- La méthode `Insert` insère un élément dans la liste à un index donné\n",
    "    - cette méthode est plus compliquée que `Add`, car elle doit faire de la place au nouvel élément\n",
    "    - on ne doit pas remplacer un élément par un nouveau. on doit plutôt déplacer des éléments vers la droite pour lui faire de la place\n",
    "    \n",
    "```\n",
    "méthode Insert(index, item):\n",
    "    // déplace les éléments à partir de index vers la droite\n",
    "    // pour faire de la place au nouvel élément\n",
    "    pour j = index jusqu'à Count-1:\n",
    "        tab[j+1] = tab[j]\n",
    "    tab[index] = item\n",
    "    Count++\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "![Tableau dynamique - insérer un élément](../images/ArrayList_insérer.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Qu'arrive-t-il quand il n'y a plus de place dans le tableau?\n",
    "- En d'autres mots, qu'arrive-t-il quand on appelle `Add` ou `Insert` et que `Count == Capacity`?\n",
    "    - *Réponse*: on doit augmenter la capacité du tableau\n",
    "    - c'est pour ça qu'on parle de **tableau dynamique**\n",
    "        - la capacité du tableau augmente au besoin\n",
    "\n",
    "```\n",
    "méthode IncreaseCapacity(newCapacity):\n",
    "    temp = nouveau tableau de taille newCapacity\n",
    "    copier les éléments de tab dans le nouveau tableau temp\n",
    "    tab = temp\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "![Tableau dynamique - augmenter la capacité](../images/ArrayList_capacité.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Comment choisir la nouvelle capacité\n",
    "- Évidemment, la nouvelle capacité doit être plus grande que l'ancienne, sinon il n'y aura pas de place pour tous les éléments\n",
    "- On doit éviter d'augmenter la capacité de seulement 1 à chaque fois parce que allouer un nouveau tableau et tout copier est dispendieux\n",
    "    - on ne veut pas appeler `IncreaseCapacity` à chaque fois qu'on appelle `Add` ou `Insert`\n",
    "- Mais on ne veut pas non plus allouer un tableau beaucoup trop grand pour éviter d'avoir un espace inutilisé trop grand\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "- Il y 2 techniques populaires:\n",
    "    - augmenter la capacité par une constante, comme par exemple ajouter 10 éléments à chaque fois qu'on appelle `IncreaseCapacity`\n",
    "    - ou multiplier la capacité par 2 à chaque fois qu'on appelle `IncreaseCapacity`\n",
    "- Si on veut minimiser l'espace perdu sans avoir à appeler `IncreaseCapacity` à chaque fois, on peut choisir la première option\n",
    "- Si on veut minimiser la complexité (le nombre d'opérations), la deuxième option est préférable\n",
    "- Dans `Add` et `Insert`, on doit donc commencer par vérifier si on a de la place dans le tableau pour un nouvel élément, et sinon, on appelle `IncreaseCapacity` avant d'ajouter ou d'insérer le nouvel élément"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### `Contains` et `IndexOf`\n",
    "- Ces 2 méthodes sont essentiellement des variations sur l'algorithme de recherche séquentielle\n",
    "- Il faut cependant faire attention d'utiliser `Count`, et non pas `Capacity`, comme limite d'index de recherche dans le tableau\n",
    "    - on doit rechercher seulement dans la partie active du tableau\n",
    "    - si on utilise `Capacity`, on va également rechercher dans l'espace libre, ou inutilisée du tableau, ce qui peut nous donner des résultats incorrects\n",
    "- La différence entre les 2 méthodes est que\n",
    "    - `Contains` retourne *vrai* si l'élément se retrouve dans le tableau, et *faux* sinon\n",
    "    - `IndexOf` retourne l'index où l'élément a été trouvé, ou -1 si l'élément n'est pas dans le tableau\n",
    "        - si l'élément se retrouve plusieurs fois dans le tableau, alors l'index de sa première occurence est retourné"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### `RemoveAt` et `Remove`\n",
    "- Ces 2 méthodes sont à l'opposé de `Insert`\n",
    "    - `RemoveAt`: enlève un élément à un index spécifique\n",
    "        - très semblable à `Insert`, sauf qu'on enlève un élément au lieu d'en insérer un\n",
    "        - on ne doit pas laisser de *trous* dans le tableau, donc on doit déplacer les éléments suivants l'index vers la gauche\n",
    "            - à moins évidemment que l'élément qu'on enlève est le dernier élément, donc il n'y a pas d'éléments suivants\n",
    "    - `Remove`: enlève un élément\n",
    "        - ici, l'index n'est pas spécifié\n",
    "        - on effectue une recherche séquentielle pour trouver l'élément, avec `IndexOf`\n",
    "        - et s'il est trouvé, on l'enlève avec `RemoveAt`\n",
    "        - on retourne *vrai* si un élément a été enlevé, ou *faux* sinon\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "![Tableau dynamique - enlever un élément](../images/ArrayList_enlever.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "```\n",
    "méthode RemoveAt(index):\n",
    "    pour j = index+1 jusqu'à Count-1:\n",
    "        tab[j-1] = tab[j]\n",
    "    Count--\n",
    "    \n",
    "méthode Remove(item):\n",
    "    index = IndexOf(item)\n",
    "    si index < 0:\n",
    "        retourne faux\n",
    "    RemoveAt(index)\n",
    "    retourne vrai\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Complexités des méthodes du tableau dynamique\n",
    "### Complexité de `IncreaseCapacity`\n",
    "- La complexité de cette méthode dépend essentiellement de la copie des éléments vers le nouveau tableau\n",
    "- S'il y a $n$ éléments dans l'ancien tableau, il faudra copier ces $n$ dans le nouveau tableau\n",
    "- Donc il y aura $n$ copies d'éléments, et on aura une complexité de $O(n)$\n",
    "- Les autres opérations seront constantes, sauf peut-être l'allocation du nouveau tableau\n",
    "    - le nouveau tableau n'a pas besoin d'être initialisé\n",
    "    - et même s'il devait être initialisé avec des valeurs par défaut, le nombre d'opérations ne serait pas plus que $O(n)$\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Complexité de `Add`\n",
    "- La complexité de `Add` dépend d'une condition importante: doit-on augmenter la capacité de tableau ou non avant l'ajout de l'élément\n",
    "- *Meilleur cas*:\n",
    "    - pas besoin d'augmenter la capacité du tableau\n",
    "    - tout ce qu'on fait est d'affecter l'élément dans le tableau et d'incrémenter `Count`\n",
    "    - donc la complexité est $O(1)$\n",
    "- *Pire cas*:\n",
    "    - on doit appeler `IncreaseCapacity` avant de faire l'affectation et l'incrémentation\n",
    "    - donc la complexité est $O(n)$ puisque `IncreaseCapacity` est $O(n)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "- *Cas moyen*:\n",
    "    - selon la méthode utilisée pour calculer la nouvelle capacité, on peut avoir, en utilisant une technique d'analyse amortie, de\n",
    "        - $O(n)$ si on utilise la méthode *capacité actuelle plus une constante*\n",
    "        - $O(1)$ si on utilise la méthode *capacité actuelle multipliée par 2*\n",
    "    - l'idée est qu'en utilisant la deuxième méthode, `IncreaseCapacity` est appelée beaucoup plus rarement sur le long terme que selon la première méthode, donc son coût est amortie sur une longue période\n",
    "        - *note*: la technique d'analyse amortie ne sera pas présentée en détail ici"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Complexité de `Insert`\n",
    "- *Meilleur cas*:\n",
    "    - si on est chanceux, alors il n'est pas nécessaire d'augmenter la capacité du tableau\n",
    "    - et si on est chanceux une deuxième fois, on doit insérer à la fin du tableau, donc aucun élément doit être déplacé pour faire place au nouvel élément\n",
    "    - donc on a $O(1)$\n",
    "- *Pire cas*:\n",
    "    - si on n'est pas chanceux, on doit augmenter la capacité du tableau, donc on a $O(n)$ pour cette partie\n",
    "    - si on n'est pas chanceux une deuxième fois, on doit insérer au début, à l'index 0, donc on doit déplacer tous les éléments pour faire de la place au nouvel élément, donc on a également $O(n)$ pour cette partie\n",
    "    - puisque $O(n)+O(n)=O(n)$, on a un pire cas linéaire"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "- *Cas moyen*:\n",
    "    - en moyenne, on va déplacer $n/2$ pour faire de la place au nouvel élément, donc on a $O(n)$ pour cette partie\n",
    "    - et des fois on a besoin d'augmenter la capacité, des fois non\n",
    "        - donc on a des fois $O(n)+O(n)$, des fois seulement $O(n)$\n",
    "        - dans les deux cas, on obtient $O(n)$\n",
    "- Du point de vue de la complexité théorique, il n'y a pas de différence si on augmente la capacité ou non\n",
    "- Mais du point de vue pratique, le temp d'exécution sera plus grand si on a besoin d'augmenter la capacité\n",
    "    - mais la complexité théorique sera la même, c'est-à-dire linéaire"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Complexité de `Contains` et `IndexOf`\n",
    "- Puisque ces 2 méthodes effectuent des recherches séquentielles, leurs complexités seront les mêmes\n",
    "    - $O(1)$ pour le meilleur cas\n",
    "    - $O(n)$ pour le pire cas et le cas moyen"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Complexité de `RemoveAt`\n",
    "- Cette méthode ressemble beaucoup à `Insert`, sauf qu'elle n'a jamais besoins d'augmenter la capacité\n",
    "    - il n'est généralement pas une bonne idée de réduire la capacité automatiquement, parce qu'en pratique, si on réduit la capacité après avoir enlevé un élément, on a de fortes chances de devoir augmenter la capacité plus tard lorsqu'on ajoute un élément\n",
    "- La complexité de la boucle est \n",
    "    - $O(n)$ au pire cas si on doit tout déplacer, comme quand l'index est 0\n",
    "    - $O(1)$ au meilleur cas si on enlève le dernier élément\n",
    "    - $O(n/2)=O(n)$ en moyenne"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Complexité de `Remove`\n",
    "- Cette méthode appelle `IndexOf`, et ensuite appelle `RemoveAt` si nécessaire\n",
    "- Si l'élément n'est pas dans le tableau, la complexité de `Remove` sera celle de `IndexOf`, donc $O(n)$\n",
    "- Si l'élément est dans le tableau, supposons à l'index 0, on aura\n",
    "    - $O(1)$ pour `IndexOf`\n",
    "    - $O(n)$ pour `RemoveAt`\n",
    "    - donc $O(n)$ au total\n",
    "- Si l'élément est dans le tableau, supposons à la fin du tableau, on aura\n",
    "    - $O(n)$ pour `IndexOf`\n",
    "    - $O(1)$ pour `RemoveAt`\n",
    "    - donc $O(n)$ au total\n",
    "- Si l'élément est dans le tableau, supposons au milieu du tableau, on aura\n",
    "    - $O(n/2)=O(n)$ pour `IndexOf`\n",
    "    - $O(n/2)=O(n)$ pour `RemoveAt`\n",
    "    - donc $O(n)+O(n)=O(n)$ au total\n",
    "- Donc dans tous les cas, on a $O(n)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Énumérateurs (ou itérateurs)\n",
    "- Les *énumérateurs* (aussi appelés *itérateurs*) sont utilisés pour parcourir les éléments d'une collection\n",
    "- Si la collection conserve l'ordre entre les éléments, comme dans un tableau et une liste en général, alors on peut parcourir les éléments de la liste par position, avec une boucle sur les index (ou positions) des éléments de la liste\n",
    "\n",
    "```\n",
    "pour i = 0 jusqu'à n-1:\n",
    "    f(liste[i])\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "- En général, une collection n'est pas nécessairement ordonnée, donc on ne peut pas parcourir les éléments par index ou position\n",
    "- On peut utiliser une boucle `foreach` (pour chaque élément)\n",
    "    - à l'interne, le compilateur va transformer la boucle `foreach` en utilisation d'un énumérateur\n",
    "    \n",
    "```\n",
    "pour chaque élément x dans la liste:\n",
    "    f(x)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    },
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "- Un énumérateur pour une liste-tableau doit contenir, à la base, \n",
    "    - une référence vers la liste-tableau\n",
    "    - un index courant\n",
    "    - méthode pour se déplacer vers l'élément suivant: `MoveNext`\n",
    "    - méthode ou propriété pour obtenir l'élément courant: `Current`\n",
    "- Normalement, un énumérateur commence avant le premier élément, et le premier appel de `MoveNext` va se déplacer vers le premier élément\n",
    "- Des appels successifs à `MoveNext` vont déplacer l'élément courant vers les deuxième, troisième, ..., éléments\n",
    "- Quand il n'y a plus d'éléments, `MoveNext` retourne *faux*, sinon elle retourne *vrai*\n",
    "- Si la liste est vide, le premier appel à `MoveNext` va retourner *faux*"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
