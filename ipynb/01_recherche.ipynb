{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Algorithmique\n",
    "- **Algorithmique**: \n",
    "    - Science des *algorithmes*, laquelle porte sur la manière de résoudre des problèmes en mettant en oeuvre des suites d'opérations élémentaires. [[1]](https://www.linternaute.fr/dictionnaire/fr/definition/algorithmique/)\n",
    "- **Algorithme**: \n",
    "    - Ensemble de règles opératoires dont l'application permet de résoudre un problème énoncé au moyen d'un nombre fini d'opérations. \n",
    "    - Un algorithme peut être traduit, grâce à un langage de programmation, en un programme exécutable par un ordinateur. [[2]](https://www.larousse.fr/dictionnaires/francais/algorithme/2238)\n",
    "- **Complexité d'algorithmes**: \n",
    "    - Évaluation et comparaison de la performance d'algorithmes\n",
    "    - 2 types de complexité:\n",
    "        - théorique (ou mathématique)\n",
    "        - empirique (ou expérimentale)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Recherche dans un tableau\n",
    "- Étant donné un tableau d'éléments, pas nécessairement triés, on doit rechercher la présence d'un élément dans le tableau\n",
    "- Si l'élément est présent, on peut soit\n",
    "    - retourner *vrai*, ou\n",
    "    - retourner l'index (ou la position) du tableau où l'élément a été trouvé\n",
    "- Si l'élément n'est pas présent dans le tableau, on peut soit\n",
    "    - retourner *faux*, ou\n",
    "    - retourner `-1`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Recherche séquentielle\n",
    "- Quand les éléments du tableau ne sont pas triés, on a pas le choix, on doit utiliser la *recherche séquentielle*\n",
    "- Le principe est simple: \n",
    "    1. on commence avec le premier élément du tableau\n",
    "    2. on compare cet élément avec l'élément qu'on veut trouver\n",
    "    3. si on a trouvé l'élément, on a terminé\n",
    "    4. sinon, on répète le processus avec l'élément suivant dans tableau\n",
    "    5. si on a comparé tous les éléments du tableau sans trouver l'élément recherché, la recherche a échouée"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "```\n",
    "fonction recherche_séquentielle(tab, cible)\n",
    "    // tab: tableau d'éléments comparables\n",
    "    // cible: l'élément recherché\n",
    "    // n: longueur de tab\n",
    "    // retour: index de cible dans le tableau, ou -1\n",
    "    pour i = 0 jusqu'à n-1:\n",
    "        si cible == tab[i]:\n",
    "            retourne i\n",
    "        \n",
    "    retourne -1\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "![Recherche séquentielle](../images/recherche_séquentielle.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Complexité d'algorithmes\n",
    "- Pour obtenir la complexité d'un algorithme, on doit normalement faire son analyse théorique\n",
    "- L'analyse empirique peut aussi être utile\n",
    "- Pour obtenir la complexité théorique d'un algorithme, pas besoin de le programmer et de l'exécuter: \n",
    "    - l'analyse se fait à partir de son pseudo-code\n",
    "    - on compte soit le nombre d'instructions à exécuter, ou un nombre d'opérations spécifiques\n",
    "        - comme le nombre d'additions ou de comparaisons, ou le nombre d'accès à des éléments d'un tableau\n",
    "    - on exprime le plus souvent la complexité en termes de la taille du problème\n",
    "        - comme par exemple le nombre d'éléments dans une liste ou la longueur d'un tableau\n",
    "        - $n$ est souvent utilisée pour dénoter la taille du problème\n",
    "    - on utilise souvent la notation $O(f(n))$\n",
    "        - $O$ se prononce souvent *grand O* (pas *grand zéro*, mais la lettre *O* majuscule)\n",
    "        - $f(n)$ est une fonction de $n$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "- Le plus important dans l'analyse théorique, c'est l'ordre de grandeur de la complexité, de la fonction $f$ en terme de $n$\n",
    "- L'analyse théorique détaillée utilise souvent des limites lorsque $n$ tend vers l'infini: $lim_{n\\rightarrow \\infty}$\n",
    "- La question la plus importante en analyse théorique:\n",
    "    - Quand $n$ deviendra de plus en plus grand, qu'arrivera-t-il avec la performance de l'algorithme?\n",
    "    - En d'autres mots: de quelle façon variera la performance de l'algorithme à mesure que la taille du problème augmentera?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Complexités courantes\n",
    "- *Constante*: $O(1)$\n",
    "- *Logarithmique*: $O(lg\\ n)$\n",
    "- *Linéaire*: $O(n)$\n",
    "- *Linéaire-logarithmique*: $O(n\\ lg\\ n)$\n",
    "- *Quadratique*: $O(n^2)$\n",
    "- *Exponentielle*: $O(2^n)$\n",
    "- *Factorielle*: $O(n!)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Trois cas d'analyse\n",
    "- L'analyse peut être faite à partir de trois cas:\n",
    "    - le meilleur cas\n",
    "    - le cas moyen\n",
    "    - le pire cas\n",
    "- On se concentre le plus souvent sur le pire cas, mais le cas moyen est aussi intéressant\n",
    "- Le meilleur cas est moins intéressant parce qu'il est normalement beaucoup plus rare que les autres cas"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Complexité de la recherche séquentielle\n",
    "#### Meilleur cas\n",
    "- si on est chanceux, on trouve l'élément recherché immédiatement, au début du tableau (`index == 0`)\n",
    "- peut importe la taille du tableau, si on est chanceux, on trouve l'élément recherché au début\n",
    "- dans ce cas, on dit que l'algorithme est *constant* ou de *temps constant* ou de *complexité constante* par que peu importe la taille du tableau, le nombre d'opérations vont être le même\n",
    "- on dénote sa complexité avec l'expression $O(1)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Pire cas\n",
    "- Si on n'est pas chanceux, on doit parcourir tout le tableau jusqu'à la dernière position (`index == n-1`), pour trouver l'élément recherché\n",
    "- On doit aussi parcourir le tableau au complet si l'élément recherché n'est pas dans le tableau\n",
    "- Le nombre d'opérations à exécuter va dépendre de $n$\n",
    "    - pour chaque élément du tableau, on compare la `cible` à cet élément\n",
    "    - si le tableau contient 10 éléments, on devra faire 10 comparaisons\n",
    "    - si le tableau contient 100 éléments, on devra faire 100 comparaisons\n",
    "    - si le tableau contient 1000 éléments, on devra faire 1000 comparaisons\n",
    "    - etc.\n",
    "- La complexité est dénotée $O(n)$ parce qu'elle dépend directement de la taille du tableau\n",
    "- Si la taille du tableau double, le nombre de comparaisons va doubler\n",
    "- On dit que la complexité $O(n)$ est *linéaire*, ou que l'algorithme est *linéaire*, parce que la fonction représentant sa complexité est une fonction linéaire\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Cas moyen\n",
    "- Le cas moyen est souvent plus difficile à exprimer\n",
    "- On doit souvent utiliser des mathématiques plus avancées pour faire l'analyse\n",
    "- Dans ce cas-ci, ce n'est pas trop difficile: en moyenne, on pourrait s'attendre à parcourir environ la moité du tableau pour trouver l'élément recherché\n",
    "    - des fois on le trouve au début, des fois au milieu, des fois à la fin\n",
    "    - en moyenne, on le trouve au milieu\n",
    "- Donc sa complexité est $O(n/2)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Question: Est-ce que $O(n) = O(n/2)$?\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "- *Réponse*: **oui**\n",
    "- Le plus important n'est pas le nombre exact d'opérations, mais plutôt la croissance du nombre d'opérations par rapport à la taille du problème\n",
    "- Peu importe si on a $O(n)$ ou $O(n/2)$, si la taille du tableau double, le nombre d'opérations va doubler\n",
    "- Si la taille du tableau est multipliée par 100, le nombre d'opérations va être multiplé par 100 aussi dans les deux cas\n",
    "- L'augmentation est *linéaire* dans les deux cas\n",
    "    - l'augmentation est une représentée par une fonction linéaire\n",
    "- Dans la notation $O(f(n))$, les constantes ne sont pas importantes\n",
    "- Donc pour la recherche séquentielle, le pire cas et le cas moyen ont la même complexité"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Question: *Peut-on faire mieux?*\n",
    "- **Non**, à moins d'ajouter certaines conditions au problème\n",
    "- Si on suppose que les éléments du tableau sont triés, alors on peut faire mieux\n",
    "- On sait alors que le plus petit élément est au début, et le plus grand à la fin\n",
    "    - en supposant un ordre croissant bien entendu\n",
    "- Donc on peut exploiter ces informations pour obtenir un algorithme plus rapide"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Recherche dichotomique\n",
    "- Si les éléments du tableau sont triés, on peut accélérer la recherche\n",
    "- La *recherche dichotomique* est un algorithme du type *diviser-pour-régner*\n",
    "- L'idée de départ est de comparer l'élément à rechercher avec l'élément qui se retrouve au milieu du tableau\n",
    "- Si on est chanceux, l'élément recherché se trouve au milieu du tableau\n",
    "- Mais en général, l'élément recherché ne se trouvera pas au milieu du tableau\n",
    "- Dans ce cas, puisque le tableau est trié, on peut éliminer la moitié du tableau de notre recherche\n",
    "    - si l'élément recherché est **plus petit** que l'élément au milieu du tableau, on peut éliminer tous les éléments dans la partie droite du tableau puisqu'ils sont plus grands que l'élément du milieu, donc aussi plus grands que l'élément recherché\n",
    "    - à l'inverse, si l'élément recherché est **plus grand** que l'élément au milieu du tableau, on peut éliminer tous les éléments dans la partie gauche du tableau puisqu'ils sont plus petits que l'élément du milieu, donc aussi plus petits que l'élément recherché\n",
    "- Après l'élimination d'une moitié de tableau, on répète la procédure avec la moitié de tableau qui reste\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "- *Notes*: \n",
    "    - l'élément du milieu n'est pas toujours exactement au milieu\n",
    "        - si la taille du tableau est impaire, alors l'élément du milieu est vraiment au milieu\n",
    "        - sinon, selon l'algorithme présenté plus loin, l'élément du milieu va être placé un peu à gauche\n",
    "            - donc la \"*moitié*\" gauche du tableau dans ce cas va être plus petite que la \"*moitié*\" droite\n",
    "    - par conséquent, on n'élimine pas toujours exactement la moitié du tableau\n",
    "    - du point de vue algorithmique, si on élimine $n/2$ ou $n/2-1$ ou $n/2+1$ éléments, il n'y aura pas de différence\n",
    "        - l'effet du $-1$ ou du $+1$ ne sera pas important quand $n$ va être grand"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "```\n",
    "fonction recherche_dichotomique(tab, cible)\n",
    "    // tab: tableau d'éléments comparables\n",
    "    // cible: l'élément recherché\n",
    "    // n: longueur de tab\n",
    "    // retour: index de cible dans le tableau, ou -1\n",
    "    début = 0\n",
    "    milieu = n / 2\n",
    "    fin = n - 1\n",
    "    tant que début <= fin:\n",
    "        si cible == tab[milieu]: // trouvé!\n",
    "            retourne milieu\n",
    "        si cible < tab[milieu]:  // la cible est plus petite que l'élément du milieu\n",
    "                                 // on doit chercher dans la partie gauche\n",
    "            fin = milieu - 1     // on déplace la fin de la partie active du tableau à gauche du milieu\n",
    "        sinon:                   // la cible est plus grande que l'élément du milieu\n",
    "                                 // on doit chercher dans la partie droite\n",
    "            debut = milieu + 1   // on déplace le début de la partie active du tableau à droite du milieu\n",
    "        milieu = (début + fin)/2\n",
    "    retourne -1\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "![Recherche dichotomique](../images/recherche_dichotomique.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Complexité de la recherche dichotomique\n",
    "#### Meilleur cas\n",
    "- Si on est chanceux, comme dans la recherche séquentielle, on trouve l'élément recherché du premier coup\n",
    "- Donc peu importe la taille du tableau, on trouve l'élément recherché en temps constant\n",
    "- Complexité: $O(1)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Pire cas\n",
    "- On doit comparer combien l'élément recherché à d'élements du tableau si on n'est pas chanceux?\n",
    "    - on doit exprimer ce nombre en fonction de la taille du tableau $n$\n",
    "- Dans le cas de la recherche séquentielle, la taille du tableau restant diminue de 1 à chaque étape si l'élément recherché n'est pas trouvé\n",
    "- Dans le cas de la recherche dichotomique, de quelle façon la taille du tableau restant varie-t-elle?\n",
    "    - à chaque étape non-fructueuse, on élimine (environ) la moitié du tableau\n",
    "    - donc la taille du tableau est coupée en 2\n",
    "    - la taille du tableau restant après chaque étape non-fructueuse est donc (environ) $n/2$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "- Combien de fois au maximum peut-on couper la taille du tableau en 2 dans l'algorithme?\n",
    "    - la boucle continue tant que `début <= fin`\n",
    "    - la boucle prendra donc fin quand on aura `début > fin`\n",
    "    - comment peut-op obtenir `début > fin`?\n",
    "        - soit en faisant `debut = milieu + 1` ou `fin = milieu - 1`\n",
    "        - si `début == fin`, et qu'on fait un de ces 2 calculs, on obtient `début > fin` \n",
    "            - parce à l'étape précédente, on a calculé `milieu = (début + fin)/2`\n",
    "            - et avec `début == fin`, `milieu == début == fin`\n",
    "    - quand `début == milieu == fin`, il n'y a qu'un élément dans le tableau restant\n",
    "        - soit on trouve l'élément recherché\n",
    "        - soit on obtient `début > fin`\n",
    "    - donc après avoir obtenu une taille de tableau restant de 1, on est certain d'arrêter\n",
    "    - par conséquent, on divise $n$ par 2 plusieurs fois, jusqu'à temps d'obtenir $n = 1$\n",
    "    - le nombre de fois qu'on peut couper la taille du tableau en 2 est donc $log_{2}n=lg\\ n$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "- **Petit rappel**: si $x = log_{2}n$, alors $2^x = n$\n",
    "- La complexité de la recherche dichotomique est donc $O(lg\\ n)$\n",
    "- On dit aussi que l'algorithme de la recherche dichotomique est **logarithmique**\n",
    "- *Exemples*:\n",
    "    - si $n = 64$, on aura besoins de couper le tableau en 2 un maximum de 6 fois parce que $lg\\ 64 = 6$\n",
    "        - ou si vous préférez $2^6 = 64$\n",
    "    - si $n = 128$, alors on a $lg\\ 128 = 7$\n",
    "    - si $n = 1024$, alors on a $lg\\ 1024 = 10$\n",
    "- Donc quand la taille du tableau double, on ajoute seulement 1 étape au pire cas\n",
    "    - c'est beaucoup mieux que la recherche séquentielle, qui double le nombre d'étapes quand la taille du tableau double\n",
    "    - mais n'oubliez pas que la recherche fonctionne seulement sur les tableaux triés, et que la recherche séquentielle fonctionne sur tous les tableaux, triés ou non"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Cas moyen\n",
    "- En moyenne, on s'attend à trouver l'élément au milieu du processus\n",
    "- Le nombre de fois qu'on coupe le tableau en 2 est divisé par 2\n",
    "- Donc en moyenne, on a $O(\\frac{1}{2} lg\\ n) = O(lg\\ n)$"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
