{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Récursivité\n",
    "## Définitions récursives\n",
    "\n",
    "- Une définition récursive est une définition qui dépend d'elle même\n",
    "- Dans le monde de l'informatique, les définitions récursives sont courantes\n",
    "- Non seulement les informaticiens utilisent la récursivité dans leur code, ils utilisent aussi les définitions récursives, aussi appelées *sigles auto-référentiels* (ou *recursive acronym* en anglais) pour nommer leurs projets\n",
    "- Exemples [[1]](https://fr.wikipedia.org/wiki/Sigles_auto-référentiels):\n",
    "    - **GNU**: *GNU's not unix*\n",
    "    - **PHP**: *PHP: Hypertext Preprocessor*\n",
    "    - **PIP**: *PIP Installs Packages*\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Fonctions mathématiques récursives\n",
    "- En théorie des nombres, les nombres entiers sont définis à l'aide d'une définition mathématique récursive, plus précisément à l'aide de la fonction successeur $s(x)$\n",
    "    - on suppose l'existence du nombre 0 et on applique $s$ le nombre de fois voulu\n",
    "        - $s(0)$\n",
    "        - $s(s(0))$\n",
    "        - $s(s(0))$\n",
    "        - ...\n",
    "    - par convention, on dénote\n",
    "        - $s(0) = 1$\n",
    "        - $s(s(0)) = s(1) = 2$\n",
    "        - $s(s(s(0))) = s(2) = 3$\n",
    "        - ...\n",
    "- $s$ est une fonction récursive primitive, et est utilisée à la base de la définition de l'addition\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Somme d'entiers positifs\n",
    "$$S=1+2+3+\\cdots +(n-2)+(n-1)+n=\\sum _{i=1}^{n}i={\\frac {n(n+1)}{2}}$$\n",
    "- Supposons qu'on ne connaît pas la formule pour calculer la somme d'entiers positifs\n",
    "- On pourrait la calculer de 2 façons:\n",
    "    - itérative (avec une boucle), ou\n",
    "    ```\n",
    "    fonction somme_itérative(n):\n",
    "        somme = 0\n",
    "        pour i = 1 jusqu'à n:\n",
    "            somme += i\n",
    "        retourne somme\n",
    "    ```\n",
    "    - récursive (avec une fonction récursive) $$\\sum _{i=1}^{n}i=n+\\sum _{i=1}^{n-1}i$$\n",
    "    ```\n",
    "    fonction somme_récursive(n):\n",
    "        si n < 1:\n",
    "            retourne 0\n",
    "        retourne n + somme_récursive(n-1)\n",
    "    ```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Complexité de `somme_itérative`\n",
    "- La complexité est simple à calculer\n",
    "    - les opérations `=` et `+=` sont constantes\n",
    "    - on a une boucle de 1 à $n$, donc il y $n$ itérations de la boucle\n",
    "    - ce qui nous donne une complexité de $O(n)$\n",
    "- Si on utilisait la formule, çà serait encore mieux parce qu'on n'aurait pas de boucle\n",
    "    - on aurait seulement une multiplication, une addition, et une division\n",
    "    - donc on aurait une complexité constante $O(1)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Complexité de `somme_récursive`\n",
    "- Il n'y a pas de boucle dans cette fonction, mais ça ne veut pas dire qu'il n'y a pas de répétitions de code\n",
    "- Pour analyser une fonction récursive, on doit compter le nombre de fois que la fonction est appelée\n",
    "- Pour simplifier la notation, supposons que la fonction `somme_récursive` est aussi nommée $f$\n",
    "- Le premier appel est $f(n)$, et si $n \\geq 1$, on appelle $f(n-1)$\n",
    "- Si $n-1 \\geq 1$, alors on appelle $f(n-2)$, et ainsi de suite, jusqu'à ce qu'on appelle $f(0)$\n",
    "    - il n'y aura pas d'appel récursif avec $f(0)$\n",
    "    - la suite d'appels récursifs s'arrête avec $f(0)$\n",
    "    - on dit que $n=0$ est un cas de base, ou une condition d'arrêt d'appels récursifs\n",
    "    - *sans cas de base* (il pourrait en avoir plusieurs), on pourrait avoir une situation de **récursivité infinie**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "- En notation mathématique, on pourrait réécrire `somme_récursive` (en utilisant le raccourci $f$ encore une fois):\n",
    "$$f(n) = \\begin{cases} \n",
    "0 & n < 1 \\\\\n",
    "n+f(n-1) & n \\geq 1\n",
    "\\end{cases}$$\n",
    "- Donc on va appeler la fonction $n-1$ fois: $f(n), f(n-1), f(n-2), ..., f(1), f(0)$\n",
    "\n",
    "![Somme récursive](../images/somme_récursive.jpg)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "- On aura donc $n+1$ appels de fonctions, chaque fonction faisant un nombre d'opérations constant (qui ne dépend pas de $n$)\n",
    "    - soit on fait une addition avec le résultat de l'appel récursif\n",
    "    - soit on fait une comparaison\n",
    "- La complexité sera de $O(n)$, comme dans la version itérative\n",
    "- Du point de vue théorique, la complexité de deux versions est identique\n",
    "- Mais du point de vue pratique, la version itérative sera plus rapide parce que faire un appel de fonction est plus lent que de faire une autre itération dans une boucle\n",
    "- Lorsque possible, on utilise une version itérative de l'algorithme\n",
    "- Mais parfois, un algorithme peut être très difficile à exprimer en version itérative, donc on doit utiliser la version récursive"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Pile d'appels de fonctions\n",
    "- À chaque fois qu'une fonction (ou méthode) est appelée, de façon récursive ou non, de l'espace est réservée pour cet appel sur la *pile d'appels* de fonctions\n",
    "- De l'espace est alloué pour les paramètres de la fonction ainsi que pour les variables locales\n",
    "- De l'information est aussi conservée pour savoir où retourner (ou d'où la fonction a été appelée\n",
    "- Chaque appel de fonction a son espace réservée sur la pile, donc même on appelle la même fonction plusieurs fois de façon récursive, chaque appel a sa propre copie de chaque paramètre et de chaque variable locale\n",
    "- Donc chaque appel de fonction n'interfère pas avec les autres, à moins que les différents appels aient tous des pointeurs vers le(s) même(s) objet(s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "![Pile d'appels de fonctions: somme récursive](../images/pile_appels.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Suite de Fibonacci\n",
    "- La *suite de Fibonacci* est définie de façon récursive sur les entiers positifs:\n",
    "$$F_n = \\begin{cases} \n",
    "0 & n = 0 \\\\\n",
    "1 & n = 1  \\\\\n",
    "F_{n-1}+F_{n-2} & n > 1\n",
    "\\end{cases}$$\n",
    "\n",
    "- Pour calculer $F_5$, on doit calculer $F_4$ et $F_3$\n",
    "    - mais pour calculer $F_4$, on doit calculer $F_3$ et $F_2$\n",
    "    - et pour calculer $F_3$, on doit calculer $F_2$ et $F_1$\n",
    "    - et on doit continuer à appliquer le même principe, qui devient compliqué, surtout si on utilise un nombre plus grand que 5 pour commencer\n",
    "- La suite de Fibonacci est aussi souvent dénoté sous forme de fonction $f(n)$ à la place de $F_n$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "![Animation Fibonacci](../images/Animation_Fibonacci.gif)\n",
    "EloiBerlinger / CC BY-SA (https://creativecommons.org/licenses/by-sa/4.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "- Le pseudocode est très simple, traduit directement de la définition récursive\n",
    "```\n",
    "fonction fibonacci_récursif(n):\n",
    "    si n == 0 ou n == 1:\n",
    "        retourne n\n",
    "    retourne fibonacci_récursif(n-1) + fibonacci_récursif(n-2)\n",
    "```\n",
    "- *Notes*: \n",
    "    - avant de pouvoir exécuter l'addition à la dernière ligne, il faut avoir obtenu le résultat des 2 appels récursifs\n",
    "    - donc le premier appel récursif `fibonacci_récursif(n-1)` va être exécuté au complet avant que l'autre appel récursif `fibonacci_récursif(n-2)` soit exécuté au complet\n",
    "    - et ensuite l'addition va être exécutée et le résultat retourné"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "![Suite de Fibonacci (n=5): appels récursifs](../images/fibo5_appels.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "![Suite de Fibonacci (n=5): retour](../images/fibo5_retour.jpg)\n",
    "*Note*: l'ordre dans lequel les appels récursifs sont faits est donnée dans les cercles noirs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Répétitions\n",
    "- La définition récursive de la suite de Fibonacci est simple\n",
    "- Mais elle n'est pas très performante\n",
    "- Il y beaucoup de répétitions: la fonction avec les mêmes valeurs de $n$ apparaissent plusieurs fois dans l'arbre des appels récursifs\n",
    "- Par exmple, si on veut représenter l'arbre d'appels récursifs pour $F_6$ à partir de celui de $F_5$\n",
    "    - on crée une nouvelle racine pour $F_6$\n",
    "    - on copie l'arbre pour $F_5$ à gauche de $F_6$\n",
    "    - et on copie l'arbre pour $F_4$ à droite de $F_6$\n",
    "- Donc pour calculer $F_6$, on répète tous les calculs de $F_5$, plus tous les calculs de $F_4$, qui eux mêmes étaient inclus dans $F_5$!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "![Suite de Fibonacci (n=6): appels récursifs](../images/fibo6_appels.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "![Suite de Fibonacci (n=6): répétitions](../images/fibo6_répétitions.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Complexité de `fibonacci_récursif`\n",
    "- L'analyse d'un algorithme récursif dépend du nombre d'appel récursif de la fonction\n",
    "- Soit $A_i$, le nombre de fois que $f(i)$ est appelé au cours de l'éxécution de $f(n)$, on a $$A_i = \\begin{cases} \n",
    "1 & i = n\\ \\text{ou}\\ i = n-1 \\\\\n",
    "A_{i+1}+A_{i+2} & 0 < n < n-1  \\\\\n",
    "A_{2} & n = 0\n",
    "\\end{cases}$$\n",
    "- $f(i)$ est appelé seulement par $f(i+1)$ et $f(i+2)$, donc le nombre de fois que $f(i)$ est appelé est la somme du nombre de fois que $f(i+1)$ et $f(i+2)$ sont appelés\n",
    "    - avec les exceptions de $f(n)$ et $f(n-1)$ qui sont appelés exactement une fois chaque\n",
    "    - et $f(0)$ qui est appelé seulement par $f(2)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "- Par exemple, lors de l'exécution de $f(6)$:\n",
    "    - $A_{6} = 1$\n",
    "    - $A_{5} = 1$\n",
    "    - $A_{4} = 1 + 1 = 2$\n",
    "    - $A_{3} = 1 + 2 = 3$\n",
    "    - $A_{2} = 2 + 3 = 5$\n",
    "    - $A_{1} = 5 + 3 = 8$\n",
    "    - $A_{0} = 5$\n",
    "- À part $A_{0}$, les nombres $A_i$ forment eux-mêmes une suite de Fibonacci!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "- Le nombre total d'appels récursifs est donc\n",
    "$$(F_1 + F_2 + F_3 + \\cdots + F_{n-1} + F_{n}) + F_{n-1} = F_{n-1} + \\sum _{i=1}^{n}F_i = O(2^n)$$\n",
    "- La complexité est donc **exponentielle**!\n",
    "- *Notes*: \n",
    "    - la preuve mathématique de cette complexité exponentielle est trop compliquée pour être présentée dans cette introduction à la récursivité et à l'analyse d'algorithmes\n",
    "    - Le principe est qu'à chaque fois qu'on ajoute 1 au paramètre $n$, on double pratiquement le nombre de calculs nécessaire\n",
    "    - Si on passe de $F_n$ à $F_{n+1}$, on refait tous les calculs de $F_{n}$ plus tous les calculs de $F_{n-1}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Fibonacci, version itérative \n",
    "- Technique de *mémoïsation*\n",
    "    - pour éviter de répéter les mêmes calculs, on peut \"*écrire un mémo*\" pour se souvenir du résultat d'un calcul\n",
    "    - on peut réutiliser le résultat du calcul au besoin, sans refaire le calcul au complet à chaque fois\n",
    "- Par exemple, si on veut calculer $F_6 = F_5 + F_4$, on pourrait calculer $F_4$ en premier, et réutiliser ce résultat lors du calcul de $F_5$\n",
    "    - mais $F_5$ et $F_4$ ont besoin de $F_3$, donc il faut aussi éviter de calculer $F_3$ plusieurs fois\n",
    "- Le plus simple est de commencer le calcul des $F_i$ par \"le bas\", c'est-à-dire avec les petites valeurs de $F_i$\n",
    "    - $F_0 = 0$ et $F_1 = 1$ sont déjà connus\n",
    "    - ensuite on calcule $F_2$ à partir de $F_0$ et $F_1$\n",
    "    - ensuite on calcule $F_3$ à partir de $F_1$ et $F_2$\n",
    "    - on a pas besoin de conserver toutes les valeurs $F_i$ calculées, seulement les 2 dernières"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "```\n",
    "fonction fibonacci_itératif(n):\n",
    "    f_i2 = 0  // pour F i-2: au début, F0, ensuite F1, F2, F3, ...\n",
    "    f_i1 = 1  // pour F i-1: au début, F1, ensuite F2, F3, F4, ...\n",
    "    si n == 0 ou n == 1:\n",
    "        retourne n\n",
    "    pour i = 2 jusqu'à n:\n",
    "        f_i = f_i1 + f_i2\n",
    "        f_i2 = f_i1       // pour la prochaine itération\n",
    "        f_i1 = f_i        // f_i1 devient f_i2, et f_i devient f_i1\n",
    "    retourne f_i\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "![Suite de Fibonacci (n=6): version itérative](../images/fibo6_itérative.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Complexité de `fibonacci_itératif`\n",
    "- L'analyse de la version itérative est beaucoup plus simple que celle de la version récursive\n",
    "- On a une boucle avec $n-1$ itérations, et toutes les autres opérations sont de complexité constante (affectations, comparaisons et addition), donc on a une complexité linéaire, $O(n)$\n",
    "- Une complexité linéaire est de loin préférable à une complexité exponentielle\n",
    "    - le temps d'exécution de $F_{100}$ sera environ 10 fois plus grand que celui de $F_{10}$ avec la version itérative\n",
    "    - mais avec la version récursive, on aura $O(2^{100})=O(2^{90}\\times 2^{10})$, donc $F_{100}$ sera environ $2^{90}=1237940039285380274899124224\\approx 1,2\\times 10^{27}$ fois plus lent que $F_{10}$!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Recherche séquentielle récursive\n",
    "```\n",
    "fonction recherche_séquentielle_récursive(tab, cible, début)\n",
    "    // tab: tableau d'éléments comparables\n",
    "    // cible: l'élément recherché\n",
    "    // début: recherche à partir de cet index\n",
    "    // n: longueur de tab\n",
    "    // retour: index de cible dans le tableau, ou -1\n",
    "    si début >= n:\n",
    "        retourne -1\n",
    "    si cible == tab[début]:\n",
    "        retourne début\n",
    "    retourne recherche_séquentielle_récursive(tab, cible, début+1)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "![Recherche séquentielle récursive](../images/recherche_séq_réc.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Recherche dichotomique récursive\n",
    "```\n",
    "fonction recherche_dichotomique_récursive(tab, cible, début, fin)\n",
    "    // tab: tableau d'éléments comparables\n",
    "    // cible: l'élément recherché\n",
    "    // début: recherche à partir de cet index\n",
    "    // fin: recherche jusqu'à cet index (inclusif)\n",
    "    // n: longueur de tab\n",
    "    // retour: index de cible dans le tableau, ou -1\n",
    "    si début > fin:\n",
    "        retourne -1\n",
    "    milieu = (début + fin)/2\n",
    "    si cible == tab[milieu]: // trouvé!\n",
    "        retourne milieu\n",
    "    si cible < tab[milieu]:  \n",
    "        retourne recherche_dichotomique_récursive(tab, cible, début, milieu-1)\n",
    "    retourne recherche_dichotomique_récursive(tab, cible, milieu+1, fin)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "![Recherche dichotomique récursive](../images/recherche_dico_réc.jpg)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}