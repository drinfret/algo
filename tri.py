def tri_par_fusion(tab, début, fin):
    if fin - début <= 0:  # fin - début + 1 <= 1
        return
    milieu = (début + fin + 1) // 2
    tri_par_fusion(tab, début, milieu - 1)
    tri_par_fusion(tab, milieu, fin)
    fusionner(tab, début, milieu, fin)


def fusionner(tab, début, milieu, fin):
    n = fin - début + 1
    temp = [0] * n

    i = début  # index dans le premier sous-tableau
    j = milieu  # index dans le deuxième sous-tableau
    k = 0  # index dans le tableau temporaire
    while k < n:
        while i < milieu and (j > fin or tab[i] <= tab[j]):
            temp[k] = tab[i]
            i += 1
            k += 1
        while j <= fin and (i >= milieu or tab[j] <= tab[i]):
            temp[k] = tab[j]
            j += 1
            k += 1

    for k in range(len(temp)):
        tab[début + k] = temp[k]


def médiane_de_3(tab, début, fin):
    milieu = début + (fin - début) // 2
    print("        médiane de 3 ({}, {}, {})".format(tab[début], tab[milieu], tab[fin]), end="")
    if tab[milieu] < tab[début]:
        tab[milieu], tab[début] = tab[début], tab[milieu]
    if tab[fin] < tab[début]:
        tab[fin], tab[début] = tab[début], tab[fin]
    if tab[milieu] < tab[fin]:
        tab[milieu], tab[fin] = tab[fin], tab[milieu]
    tab[début], tab[fin] = tab[fin], tab[début]
    print(" -> {}".format(tab[début]))


def tri_très_rapide(tab, début, fin):
    if fin - début <= 0:
        return
    print("avant médiane début->{} fin->{}:   tab->{}".format(début, fin, tab))
    médiane_de_3(tab, début, fin)
    print("avant partition début->{} fin->{}:   tab->{}".format(début, fin, tab))
    p, q = partition(tab, début, fin)
    print("après partition début->{} fin->{}:   p->{} q->{} pivot->{} tab->{}".format(début, fin, p, q, tab[p], tab))
    tri_très_rapide(tab, début, p - 1)
    tri_très_rapide(tab, q, fin)


def partition(tab, début, fin):
    pivot = tab[début]
    gauche = début
    milieu = gauche
    droite = fin
    print("        pivot->{}".format(pivot))
    while milieu <= droite:
        if tab[milieu] < pivot:
            tab[gauche], tab[milieu] = tab[milieu], tab[gauche]
            gauche += 1
            milieu += 1
        elif tab[milieu] > pivot:
            tab[milieu], tab[droite] = tab[droite], tab[milieu]
            droite -= 1
        else:
            milieu += 1
    return gauche, milieu


# tab = [7, 2, 5, 8, 3, 1, 4, 6, 9]
# print(tab)
# tri_par_fusion(tab, 0, len(tab)-1)
# print(tab)

# tab = [7, 2, 5, 8, 9, 1, 4, 6, 3]
# tab = [5, 2, 4, 10, 12, 1, 9, 7, 3, 8, 11, 6]
tab = [5, 2, 5, 9, 11, 7, 4, 10, 12, 1, 9, 7, 9, 11, 11, 9, 5, 3, 8, 11, 6]
print(tab)
tri_très_rapide(tab, 0, len(tab)-1)
print(tab)