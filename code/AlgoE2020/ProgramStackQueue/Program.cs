﻿using System;
using StackQueue;

namespace ProgramStackQueue
{
    class Program
    {
        static void Main()
        {
            ArrayQueue<int> q1 = new ArrayQueue<int>();
            q1.Enqueue(4);
            q1.Enqueue(2);
            q1.Enqueue(6);
            while (q1.Count > 0)
            {
                Console.WriteLine("Dequeue {0}", q1.Dequeue());
            }
            q1.Enqueue(4);
            q1.Enqueue(2);
            q1.Enqueue(6);
            q1.Dequeue();
            q1.Dequeue();
            q1.Enqueue(1);
            q1.Enqueue(3);
            q1.Enqueue(5);
            while (q1.Count > 0)
            {
                Console.WriteLine("Dequeue {0}", q1.Dequeue());
            }
        }
    }
}