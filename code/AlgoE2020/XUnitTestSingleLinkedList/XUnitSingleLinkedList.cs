using Xunit;
using LinkedList;

namespace XUnitTestSingleLinkedList
{
    public class XUnitTestsSingleLinkedList
    {
        #region Constructor

        [Fact]
        public void SingleLinkedList_InitialListIsEmpty()
        {
            // ReSharper disable once CollectionNeverUpdated.Local
            var list = new SingleLinkedList<int>();
            Assert.Empty(list);
        }
        #endregion

        #region Add
        [Fact]
        public void SingleLinkedList_Add4ToEmptyList_ListIsSingle()
        {
            var list = new SingleLinkedList<int>() {4};
            Assert.Single(list);
        }

        [Fact]
        public void SingleLinkedList_Add3ElementsToEmptyList_ListCountEqual3()
        {
            var list = new SingleLinkedList<int>() {4, 7, 2};
            Assert.Equal(3, list.Count);
        }

        [Fact]
        public void SingleLinkedList_Add4ToEmptyList_FirstElementEquals4()
        {
            var list = new SingleLinkedList<int>() {4};
            Assert.Equal(4, list[0]);
        }

        [Fact]
        public void SingleLinkedList_Add4And7And2ToEmptyList_FirstElementEquals4()
        {
            var list = new SingleLinkedList<int>() {4, 7, 2};
            Assert.Equal(4, list[0]);
        }

        [Fact]
        public void SingleLinkedList_Add4And7And2ToEmptyList_ThirdElementEquals2()
        {
            var list = new SingleLinkedList<int>() {4, 7, 2};
            Assert.Equal(2, list[2]);
        }
        #endregion

        #region Clear
        [Fact]
        public void SingleLinkedList_EmptyList_Clear_ListIsEmpty()
        {
            // ReSharper disable once CollectionNeverUpdated.Local
            var list = new SingleLinkedList<int>();
            list.Clear();
            Assert.Empty(list);
        }

        [Fact]
        public void SingleLinkedList_Add3ElementsToEmptyList_Clear_ListIsEmpty()
        {
            var list = new SingleLinkedList<int>() {4, 7, 2};
            list.Clear();
            Assert.Empty(list);
        }
        #endregion

        #region Contains
        [Fact]
        public void SingleLinkedList_EmptyList_Contains2IsFalse()
        {
            // ReSharper disable once CollectionNeverUpdated.Local
            var list = new SingleLinkedList<int>();
            Assert.DoesNotContain(2, list);
        }

        [Fact]
        public void SingleLinkedList_Add4And7_Contains2IsFalse()
        {
            var list = new SingleLinkedList<int>() {4, 7};
            Assert.DoesNotContain(2, list);
        }

        [Fact]
        public void SingleLinkedList_Add4And7And2_Contains2IsTrue()
        {
            var list = new SingleLinkedList<int>() {4, 7, 2};
            Assert.Contains(2, list);
        }

        [Fact]
        public void SingleLinkedList_Add2And4And7_Contains2IsTrue()
        {
            var list = new SingleLinkedList<int>() {2, 4, 7};
            Assert.Contains(2, list); // Assert.Contains(2, list);
        }
        #endregion
    }
}
