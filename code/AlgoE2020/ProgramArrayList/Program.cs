﻿using System;
using System.Linq;
using ArrayList;

namespace ProgramArrayList
{
    class Program
    {
        public static void Swap1(int x, int y)
        {
            int temp = x;
            x = y;
            y = temp;
        }

        public static void Swap2(ref int x, ref int y)
        {
            int temp = x;
            x = y;
            y = temp;
        }

        public static void Main(string[] args)
        {
            int i = 5;
            int j = 7;

            Console.WriteLine($"Before Swap1: x = {i}, y = {j}");
            Swap1(i, j);
            Console.WriteLine($"After Swap1: x = {i}, y = {j}");

            Console.WriteLine($"\nBefore Swap2: x = {i}, y = {j}");
            Swap2(ref i, ref j);
            Console.WriteLine($"After Swap2: x = {i}, y = {j}");

            ArrayList<int> listInts = new ArrayList<int>();
            listInts.Add(4);
            listInts.Add(7);
            listInts.Add(2);
            foreach (int x in listInts)
            {
                Console.WriteLine(x);
            }
            var listStrings = new ArrayList<string>(4) {"Algorithme", "Structures de données", "C#"};
            Console.WriteLine(listStrings);
            // appel implicite de GetEnumerator() dans le foreach
            foreach (var x in listStrings)
            {
                Console.WriteLine(x);
            }
            // Join appelle aussi GetEnumerator()
            Console.WriteLine("[{0}]", string.Join(" _ ", listStrings));
            
            // autre utilisation de GetEnumerator(), avec un filtre Where
            var filtered = listStrings.Where(x => x.Length > 5);
            var filteredStrings = new ArrayList<string>(filtered);
            Console.WriteLine("[{0}]", string.Join(" - ", filteredStrings));
            var filteredInts = new ArrayList<int>(listInts.Where(x => x < 5));
            Console.WriteLine("[{0}]", string.Join(", ", filteredInts));
            
            Console.WriteLine(listInts[^1]);
        }
    }
}
