﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinkedList;

namespace ProgramLinkedList
{
    class Program
    {
        static void PrintEnumerable<T>(IEnumerable<T> x) => Console.WriteLine(string.Join(" => ", x));

        static void Main()
        {
            SingleLinkedList<int> list1 = new SingleLinkedList<int>() {4, 9, 2, 5};
            foreach (int i in list1)
            {
                Console.WriteLine(i);
            }

            PrintEnumerable(list1);
            list1.Add(1);
            PrintEnumerable(list1);
            list1.Insert(0, 3);
            PrintEnumerable(list1);
            list1.Insert(2, 8);
            PrintEnumerable(list1);
            list1.RemoveAt(0);
            PrintEnumerable(list1);
            list1.RemoveAt(5);
            PrintEnumerable(list1);
            list1.RemoveAt(2);
            PrintEnumerable(list1);
            Console.WriteLine(list1.IndexOf(8));
            Console.WriteLine(list1.Remove(8));
            PrintEnumerable(list1);
            Console.WriteLine(list1.IndexOf(3));
            Console.WriteLine(list1.Remove(3));
            PrintEnumerable(list1);

            // SingleLinkedList<int> list2 = new SingleLinkedList<int>(list1);
            SingleLinkedList<int> list2 = new SingleLinkedList<int>();
            PrintEnumerable(list2);
            foreach (var x in list1)
            {
                list2.Add(x);
            }

            PrintEnumerable(list2);

            PrintEnumerable(list1.Where(x => x > 3));
            PrintEnumerable(from x in list1 where x > 3 select -x);

            // SingleLinkedList<int> list3 = new SingleLinkedList<int>(list1.Where(x => x > 3));
            SingleLinkedList<int> list3 = new SingleLinkedList<int>();
            foreach (var x in list1.Where(x => x > 3))
            {
                list3.Add(x);
            }

            PrintEnumerable(list3);

            // SingleLinkedList<int> list4 = new SingleLinkedList<int>(list1.Where(x => x > 3));
            // PrintEnumerable(list4);
        }
    }
}