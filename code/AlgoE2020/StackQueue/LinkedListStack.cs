﻿using System.Collections;
using System.Collections.Generic;
using LinkedList;

namespace StackQueue
{
    public class LinkedListStack<T> : IStack<T>
    {
        private readonly SingleLinkedList<T> _list;

        public LinkedListStack()
        {
            _list = new SingleLinkedList<T>();
        }

        public int Count
        {
            get => _list.Count;
        }

        public bool IsReadOnly => false;

        public IEnumerator<T> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(T item)
        {
            Push(item);
        }

        public void Clear()
        {
            _list.Clear();
        }

        public bool Contains(T item)
        {
            return _list.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            return _list.Remove(item);
        }

        public void Push(T item)
        {
            _list.Insert(0, item);
        }

        public T Peek()
        {
            if (_list.Count == 0)
            {
                throw new EmptyStackException();
            }

            return _list[0];
        }

        public T Pop()
        {
            if (_list.Count == 0)
            {
                throw new EmptyStackException();
            }

            T temp = _list[0];
            _list.RemoveAt(0);
            return temp;
        }
    }
    
}