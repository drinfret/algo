﻿using System;
using System.Collections.Generic;

namespace StackQueue
{
    // ReSharper disable once InconsistentNaming
    public class ArrayQueue<T> : IQueue<T>
    {
        public const int DefaultCapacity = 10;
        public int Count { get; private set; }
        public int Capacity { get; private set; }

        private T[] _array;
        private int _first;
        private int _last;

        public ArrayQueue(int initialCapacity = -1)
        {
            Capacity = initialCapacity <= 0 ? DefaultCapacity : initialCapacity;
            Count = 0;
            _first = 0;
            _last = 0;
            _array = new T[Capacity];
        }

        public ArrayQueue(IEnumerable<T> values) : this()
        {
            foreach (var value in values)
            {
                Enqueue(value);
            }
        }

        private void IncreaseCapacity(int newCapacity = -1)
        {
            if (newCapacity <= 0)
            {
                newCapacity = 2 * Capacity;
            }
            else
            {
                if (newCapacity < Count)
                {
                    throw new ArgumentException("The new capacity cannot be less than the Count");
                }
            }

            T[] newArray = new T[newCapacity];
            for (int i = 0, j = _first; i < Count; i++, j = (j + 1) % Capacity)
            {
                newArray[i] = _array[j];
            }

            Capacity = newCapacity;
            _array = newArray;
            _first = 0;
            _last = Count - 1;
        }

        public void Enqueue(T item)
        {
            if (Count >= Capacity)
            {
                IncreaseCapacity();
            }

            if (Count > 0)
            {
                _last = (_last + 1) % Capacity;
            }

            _array[_last] = item;
            Count++;
        }

        public T Dequeue()
        {
            if (Count == 0)
            {
                throw new EmptyQueueException();
            }

            T temp = _array[_first];
            _array[_first] = default;
            Count--;
            if (Count > 0)
            {
                _first = (_first + 1) % Capacity;
            }

            return temp;
        }

        public T Peek()
        {
            if (Count == 0)
            {
                throw new EmptyQueueException();
            }

            return _array[_first];
        }
        
        public override string ToString()
        {
            return $"({Count}, {Capacity})[{string.Join(", ", _array)}]";
        }
    }

    public class EmptyQueueException : Exception
    {
    }
}