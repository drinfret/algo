﻿using System.Collections;
using System.Collections.Generic;
using ArrayList;

namespace StackQueue
{
    // ReSharper disable once InconsistentNaming
    public class ArrayStack<T> : IStack<T>
    {
        private readonly ArrayList<T> _list;

        public ArrayStack()
        {
            _list = new ArrayList<T>();
        }

        public int Count
        {
            get => _list.Count;
        }

        public bool IsReadOnly => false;

        public IEnumerator<T> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(T item)
        {
            Push(item);
        }

        public void Clear()
        {
            _list.Clear();
        }

        public bool Contains(T item)
        {
            return _list.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            return _list.Remove(item);
        }

        public void Push(T item)
        {
            _list.Add(item);
        }

        public T Peek()
        {
            if (_list.Count == 0)
            {
                throw new EmptyStackException();
            }

            return _list[Count-1];
        }

        public T Pop()
        {
            if (_list.Count == 0)
            {
                throw new EmptyStackException();
            }

            T temp = _list[Count-1];
            _list.RemoveAt(Count-1);
            return temp;
        }
    }
    
}