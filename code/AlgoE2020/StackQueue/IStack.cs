﻿using System;
using System.Collections.Generic;

namespace StackQueue
{
    public interface IStack<T> : ICollection<T>
    {
        public void Push(T item);

        public T Peek();

        public T Pop();
    }
    
    public class EmptyStackException : Exception
    {
    }
}