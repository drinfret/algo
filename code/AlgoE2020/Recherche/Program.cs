﻿using System;

namespace Recherche
{
    class Program
    {
        // Versions avec tableau d'entiers
        //static int RechercheSeqentielle(int[] tab, int cible)
        //{
        //    for (int i = 0; i < tab.Length; i++)
        //    {
        //        if (tab[i] == cible)
        //        {
        //            return i;
        //        }
        //    }
        //    return -1;
        //}

        //static int RechercheDichotomique(int[] tab, int cible)
        //{
        //    // partie active du tableau: du début à la fin du tableau donné
        //    int debut = 0;
        //    int milieu = tab.Length / 2;
        //    int fin = tab.Length - 1;

        //    while (debut <= fin)          // tant qu'il y a au moins un élément dans la partie active du tableau
        //    {
        //        if (cible == tab[milieu]) // trouvé!
        //        {
        //            return milieu;
        //        }
        //        else // continue la recherche
        //        {
        //            if (cible < tab[milieu]) // la cible est plus petite que l'élément du milieu
        //            {                        // on doit chercher dans la partie gauche
        //                fin = milieu - 1;    // on déplace la fin de la partie active du tableau à gauche du milieu
        //            }
        //            else                     // la cible est plus grande que l'élément du milieu
        //            {                        // on doit chercher dans la partie droite
        //                debut = milieu + 1;  // on déplace le début de la partie active du tableau à droite du milieu
        //            }
        //            milieu = (debut + fin) / 2;
        //        }
        //    }
        //    return -1;
        //}



        // Versions avec tableau générique
        static int RechercheSequentielle<T>(T[] tab, T cible)
        {
            for (int i = 0; i < tab.Length; i++)
            {
                if (tab[i].Equals(cible))
                {
                    return i;
                }
            }
            return -1;
        }

        static int RechercheDichotomique<T>(T[] tab, T cible) 
            where T : IComparable<T>
        {
            // partie active du tableau: du début à la fin du tableau donné
            int debut = 0;
            int milieu = tab.Length / 2;
            int fin = tab.Length - 1;

            while (debut <= fin)          // tant qu'il y a au moins un élément dans la partie active du tableau
            {
                if (cible.CompareTo(tab[milieu]) == 0) // trouvé!
                {
                    return milieu;
                }
                else // continue la recherche
                {
                    if (cible.CompareTo(tab[milieu]) < 0) // la cible est plus petite que l'élément du milieu
                    {                        // on doit chercher dans la partie gauche
                        fin = milieu - 1;    // on déplace la fin de la partie active du tableau à gauche du milieu
                    }
                    else                     // la cible est plus grande que l'élément du milieu
                    {                        // on doit chercher dans la partie droite
                        debut = milieu + 1;  // on déplace le début de la partie active du tableau à droite du milieu
                    }
                    milieu = (debut + fin) / 2;
                }
            }
            return -1;
        }


        // Versions avec fonction comparateur
        // ReSharper disable once ArrangeTypeMemberModifiers
        // ReSharper disable once UnusedMember.Local
        static int RechercheSequentielle<T>(T[] tab, T cible, 
            Func<T, T, int> comparateur)
        {
            for (int i = 0; i < tab.Length; i++)
            {
                if (comparateur(tab[i], cible) == 0)
                {
                    return i;
                }
            }
            return -1;
        }

        static int RechercheDichotomique<T>(T[] tab, T cible, 
                                                      Func<T, T, int> comparateur)
        {
            // partie active du tableau: du début à la fin du tableau donné
            int debut = 0;
            int milieu = tab.Length / 2;
            int fin = tab.Length - 1;

            while (debut <= fin)          // tant qu'il y a au moins un élément dans la partie active du tableau
            {
                if (comparateur(cible, tab[milieu]) == 0) // trouvé!
                {
                    return milieu;
                }
                else // continue la recherche
                {
                    if (comparateur(cible, tab[milieu]) < 0) // la cible est plus petite que l'élément du milieu
                    {                        // on doit chercher dans la partie gauche
                        fin = milieu - 1;    // on déplace la fin de la partie active du tableau à gauche du milieu
                    }
                    else                     // la cible est plus grande que l'élément du milieu
                    {                        // on doit chercher dans la partie droite
                        debut = milieu + 1;  // on déplace le début de la partie active du tableau à droite du milieu
                    }
                    milieu = (debut + fin) / 2;
                }
            }
            return -1;
        }


        static void Main()
        {
            // NOTE: normalement, des tests unitaires devraient être utilisés pourtester ces méthodes
            // Ils ne sont pas utilisés ici pour alléger la présentation

            // avec tableau d'entiers
            int[] tab1 = { 3, 1, 7, 4, 8, 9, 2 };
            int cible = 9;
            for (int i = 0; i < tab1.Length; i++)
            {
                if (tab1[i] == cible)
                {
                    Console.WriteLine($"{cible} trouvé à la position {i}!");
                    break;
                }
            }
            int pos = RechercheSequentielle(tab1, cible);
            if (pos >= 0)
            {
                Console.WriteLine($"{cible} trouvé à la position {pos}!");
            }
            else
            {
                Console.WriteLine($"Incapable de trouver {cible} dans le tableau.");
            }

            int[] tab2 = new int[tab1.Length];
            Array.Copy(tab1, tab2, tab1.Length);
            Array.Sort(tab2);   
            Console.WriteLine("[{0}]", string.Join(", ", tab2));
            pos = RechercheDichotomique(tab2, cible);
            if (pos >= 0)
            {
                Console.WriteLine($"{cible} trouvé à la position {pos}!");
            }
            else
            {
                Console.WriteLine($"Incapable de trouver {cible} dans le tableau.");
            }


            // avec tableau de Points
            TwoDPoint[] tab3 = { new TwoDPoint(3, 1), new TwoDPoint(1, 7), new TwoDPoint(7, 4), new TwoDPoint(4, 8), new TwoDPoint(9, 2) };
            TwoDPoint cible3 = new TwoDPoint(9, 2);
            for (int i = 0; i < tab3.Length; i++)
            {
                if (tab3[i].Equals(cible3))
                //if (tab3[i] == cible3)
                {
                    Console.WriteLine($"{cible3} trouvé à la position {i}!");
                    break;
                }
            }
            int pos3 = RechercheSequentielle(tab3, cible3);
            //int pos3 = RechercheSeqentielle(tab3, cible3, (p1, p2) => p1.X.CompareTo(p2.X));
            if (pos3 >= 0)
            {
                Console.WriteLine($"{cible3} trouvé à la position {pos3}!");
            }
            else
            {
                Console.WriteLine($"Incapable de trouver {cible3} dans le tableau.");
            }

            TwoDPoint[] tab4 = new TwoDPoint[tab3.Length];
            Array.Copy(tab3, tab4, tab3.Length);
            //Array.Sort(tab4);
            Array.Sort(tab4, (p1, p2) => p1.X.CompareTo(p2.X));
            // ReSharper disable once CoVariantArrayConversion
            Console.WriteLine("[{0}]", string.Join(", ", (object[])tab4));
            //pos3 = RechercheDichotomique(tab4, cible3);
            pos3 = RechercheDichotomique(tab4, cible3, (p1, p2) => p1.X.CompareTo(p2.X));
            if (pos3 >= 0)
            {
                Console.WriteLine($"{cible3} trouvé à la position {pos3}!");
            }
            else
            {
                Console.WriteLine($"Incapable de trouver {cible3} dans le tableau.");
            }
        }
    }


}



