﻿
using System;

/* 
 * Attention! Les méthodes Equals et operator== ne sont pas équivalentes, ce qui peut être dangereux.
 * Equals compare les valeurs de X et de Y directement: les 2 valeurs de X doivent être égales, et les 2 valeurs de Y doivent 
 * aussi être égales.
 * Mais operator==, ainsi que les autres opérateurs et CompareTo, sont basées sur la longueur du vecteur représenté par le point.
 * Cette classe est utilisée pour démontrer certaines fonctionalités (Equals et CompareTo pour les algortihmes de recherche et de
 * tri), mais son utilisation générale peut être dangereuse
 * 
 */

namespace Recherche
{
    // basé sur: https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/statements-expressions-operators/how-to-define-value-equality-for-a-type
    class TwoDPoint : IEquatable<TwoDPoint>, IComparable<TwoDPoint>
    {
        // Readonly auto-implemented properties.
        public int X { get; }
        public int Y { get; }

        // Set the properties in the constructor.
        public TwoDPoint(int x, int y)
        {
            if ((x < 1) || (x > 2000) || (y < 1) || (y > 2000))
            {
                throw new ArgumentException("Point must be in range 1 - 2000");
            }
            this.X = x;
            this.Y = y;
        }

        public int Length()
        {
            return Math.Abs(this.X) + Math.Abs(this.Y);
        }

        public override string ToString()
        {
            return $"({this.X}, {this.Y})";
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as TwoDPoint);
        }

        public bool Equals(TwoDPoint p)
        {
            // If parameter is null, return false.
            if (Object.ReferenceEquals(p, null))
            {
                return false;
            }

            // Optimization for a common success case.
            if (Object.ReferenceEquals(this, p))
            {
                return true;
            }

            // If run-time types are not exactly the same, return false.
            if (this.GetType() != p.GetType())
            {
                return false;
            }

            // Return true if the fields match.
            // Note that the base class is not invoked because it is
            // System.Object, which defines Equals as reference equality.
            return (X == p.X) && (Y == p.Y);
        }

        public override int GetHashCode()
        {
            return X * 0x00010000 + Y;
        }

        public int CompareTo(TwoDPoint other)
        {
            if (other is null) return 1;

            return this.Length().CompareTo(other.Length());
        }

        // Define the is greater than operator.
        public static bool operator >(TwoDPoint operand1, TwoDPoint operand2)
        {
            return operand1.CompareTo(operand2) == 1;
        }

        // Define the is less than operator.
        public static bool operator <(TwoDPoint operand1, TwoDPoint operand2)
        {
            return operand1.CompareTo(operand2) == -1;
        }

        // Define the is greater than or equal to operator.
        public static bool operator >=(TwoDPoint operand1, TwoDPoint operand2)
        {
            return operand1.CompareTo(operand2) >= 0;
        }

        // Define the is less than or equal to operator.
        public static bool operator <=(TwoDPoint operand1, TwoDPoint operand2)
        {
            return operand1.CompareTo(operand2) <= 0;
        }

        public static bool operator ==(TwoDPoint lhs, TwoDPoint rhs)
        {
            return lhs.CompareTo(rhs) == 0;
        }

        public static bool operator !=(TwoDPoint lhs, TwoDPoint rhs)
        {
            return !(lhs == rhs);
        }
    }

}
