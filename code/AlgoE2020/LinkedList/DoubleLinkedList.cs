﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace LinkedList
{
    public class DoubleLinkedList<T> : IList<T>
    {
        public DoubleLinkedList()
        {
            Count = 0;
            First = null;
            Last = null;
        }

        internal class Node
        {
            public Node(T element, Node previous = null, Node next = null)
            {
                Element = element;
                Previous = previous;
                Next = next;
            }

            public T Element { get; set; }
            public Node Previous { get; set; }
            public Node Next { get; set; }
        }

        internal Node First { get; set; }
        internal Node Last { get; set; }
        public int Count { get; private set; }
        public bool IsReadOnly => false;

        public IEnumerator<T> GetEnumerator()
        {
            return new DoubleLinkedListEnumerator<T>(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(T item)
        {
            if (Last == null)
            {
                First = Last = new Node(item);
            }
            else
            {
                Last.Next = new Node(item, Last);
                Last = Last.Next;
            }

            Count++;
        }

        public void Clear()
        {
            Count = 0;
            First = null;
            Last = null;
        }

        public bool Contains(T item)
        {
            return IndexOf(item) != -1;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            Node current = First;
            while (current != null)
            {
                array[arrayIndex] = current.Element;
                current = current.Next;
                arrayIndex++;
            }
        }

        public bool Remove(T item)
        {
            Node current = First;
            while (current != null)
            {
                if (current.Element.Equals(item))
                {
                    if (current.Previous == null)
                    {
                        First = current.Next;
                        First.Previous = null;
                    }
                    else
                    {
                        current.Previous.Next = current.Next;
                        current.Next.Previous = current.Previous;
                    }

                    if (Last == current)
                    {
                        Last = Last.Previous;
                    }

                    current.Previous = current.Next = null;
                    Count--;
                    return true;
                }
                
                current = current.Next;
            }

            return false;
        }


        public int IndexOf(T item)
        {
            Node current = First;
            int index = 0;
            while (current != null)
            {
                if (current.Element.Equals(item))
                {
                    return index;
                }

                current = current.Next;
                index++;
            }

            return -1;
        }

        private Node NodeAt(int index)
        {
            Node current;
            if (index < Count / 2)
            {
                current = First;
                for (int i = 0; i < index && current != null; i++, current = current.Next)
                {
                }
            }
            else
            {
                current = Last;
                for (int i = Count-1; i > index && current != null; i--, current = current.Previous)
                {
                }
            }
            return current;

            
        }

        public void Insert(int index, T item)
        {
            if (0 > index || index > Count)
            {
                throw new IndexOutOfRangeException();
            }

            if (index == Count)
            {
                Add(item);
            }
            else if (index == 0)
            {
                First = new Node(item, null, First);
                Count++;
            }
            else
            {
                Node previous = NodeAt(index);
                previous.Next = new Node(item, previous, previous.Next);
                previous.Next.Previous = previous;
                Count++;
            }
        }

        public void RemoveAt(int index)
        {
            // copie légèrement modifiée de la méthode sur la liste chaînée simple
            // elle peut être améliorée, surtout quand on doit enlever le dernier élément de la liste
            if (0 > index || index >= Count)
            {
                throw new IndexOutOfRangeException();
            }

            if (index == 0)
            {
                Node temp = First;
                First = First.Next;
                First.Previous = null;
                temp.Next = null;
                Count--;
            }
            else
            {
                Node previous = NodeAt(index);
                Node temp = previous.Next;
                previous.Next = temp.Next;
                temp.Next.Previous = previous;
                temp.Previous = temp.Next = null;
                Count--;
            }
        }

        public T this[int index]
        {
            get => 0 <= index && index < Count ? NodeAt(index).Element : throw new IndexOutOfRangeException();
            set
            {
                if (0 <= index && index < Count)
                {
                    NodeAt(index).Element = value;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
        }
    }
}