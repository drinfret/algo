﻿using System.Collections;
using System.Collections.Generic;

namespace LinkedList
{
    public class DoubleLinkedListEnumerator<T> : IEnumerator<T>
    {
        private readonly DoubleLinkedList<T> _list;
        private DoubleLinkedList<T>.Node _currentNode;

        public DoubleLinkedListEnumerator(DoubleLinkedList<T> list)
        {
            _list = list;
            _currentNode = null;
        }

        public bool MoveNext()
        {
            _currentNode = _currentNode == null ? _list.First : _currentNode.Next;
            return _currentNode != null;
        }

        public void Reset()
        {
            _currentNode = null;
        }

        public T Current { get => _currentNode.Element; }

        object IEnumerator.Current => Current;

        public void Dispose()
        {
        }
    }
}