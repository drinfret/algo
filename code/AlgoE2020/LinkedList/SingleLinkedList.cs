﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace LinkedList
{
    public class SingleLinkedList<T> : IList<T>
    {
        public SingleLinkedList()
        {
            Count = 0;
            First = null;
            Last = null;
        }

        internal class Node
        {
            public Node(T element, Node next = null)
            {
                Element = element;
                Next = next;
            }

            public T Element { get; set; }
            public Node Next { get; set; }
        }

        internal Node First { get; set; }
        internal Node Last { get; set; }
        public int Count { get; private set; }
        public bool IsReadOnly => false;

        public IEnumerator<T> GetEnumerator()
        {
            return new SingleLinkedListEnumerator<T>(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(T item)
        {
            if (Last is null)
            {
                First = Last = new Node(item);
            }
            else
            {
                Last.Next = new Node(item);
                Last = Last.Next;    
            }
            Count++;
        }

        public void Clear()
        {
            Count = 0;
            First = null;
            Last = null;
        }

        public bool Contains(T item)
        {
            return IndexOf(item) != -1;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            Node current = First;
            while (current != null)
            {
                array[arrayIndex] = current.Element;
                current = current.Next;
                arrayIndex++;
            }
        }

        public bool Remove(T item)
        {
            Node current = First;
            Node previous = null;
            while (current != null)
            {
                if (current.Element.Equals(item))
                {
                    if (previous == null)
                    {
                        First = current.Next;
                    }
                    else
                    {
                        previous.Next = current.Next;
                    }

                    if (Last == current)
                    {
                        Last = previous;
                    }

                    current.Next = null;
                    Count--;
                    return true;
                }

                previous = current;
                current = current.Next;
            }

            return false;
        }


        public int IndexOf(T item)
        {
            Node current = First;
            int index = 0;
            while (current != null)
            {
                if (current.Element.Equals(item))
                {
                    return index;
                }

                current = current.Next;
                index++;
            }

            return -1;
        }

        internal Node NodeAt(int index)
        {
            if (0 > index || index >= Count)
            {
                throw new IndexOutOfRangeException();
            }
            
            Node current = First;
            for (int i = 0; i < index; i++, current = current.Next)
            {
            }

            return current;
        }

        public void Insert(int index, T item)
        {
            if (0 > index || index > Count)
            {
                throw new IndexOutOfRangeException();
            }

            if (index == Count) // include case when list is empty
            {
                Add(item);
            }
            else if (index == 0)
            {
                First = new Node(item, First);
                Count++;
            }
            else
            {
                Node previous = NodeAt(index - 1);
                previous.Next = new Node(item, previous.Next);
                Count++;
            }
        }

        public void RemoveAt(int index)
        {
            if (0 > index || index >= Count)
            {
                throw new IndexOutOfRangeException();
            }

            if (index == 0)
            {
                Node temp = First;
                First = First.Next;
                temp.Next = null;
                Count--;
            }
            else
            {
                Node previous = NodeAt(index - 1);
                if (Last == previous.Next)
                {
                    Last = previous;
                }
                Node temp = previous.Next;
                previous.Next = temp.Next;
                temp.Next = null;
                Count--;
            }
        }

        public T this[int index]
        {
            get => 0 <= index && index < Count ? NodeAt(index).Element : throw new IndexOutOfRangeException();
            set {
                if (0 <= index && index < Count)
                {
                    NodeAt(index).Element = value;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
        }
    }
}