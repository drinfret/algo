﻿using System.Collections;
using System.Collections.Generic;

namespace LinkedList
{
    public class SingleLinkedListEnumerator<T> : IEnumerator<T>
    {
        private readonly SingleLinkedList<T> _list;
        private SingleLinkedList<T>.Node _currentNode;

        public SingleLinkedListEnumerator(SingleLinkedList<T> list)
        {
            _list = list;
            _currentNode = null;
        }

        public bool MoveNext()
        {
            _currentNode = _currentNode == null ? _list.First : _currentNode.Next;
            return _currentNode != null;
        }

        public void Reset()
        {
            _currentNode = null;
        }

        public T Current { get => _currentNode.Element; }

        object IEnumerator.Current => Current;

        public void Dispose()
        {
        }
    }
}