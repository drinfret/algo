﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ArrayList
{
    // ReSharper disable once InconsistentNaming
    public class ArrayList<T> : IList<T>
    {

        private T[] _tab;

        public int Count { get; private set; }

        public int Capacity { get; private set; }

        public bool IsReadOnly => false;

        public const int DefaultCapacity = 10;

        public ArrayList(int capacity = -1)
        {
            Capacity = capacity <= 0 ? DefaultCapacity : capacity;
            Count = 0;
            _tab = new T[Capacity];
        }

        public ArrayList(IEnumerable<T> values) : this()
        {
            foreach (var value in values)
            {
                Add(value);
            }
        }

        public T this[int index]
        {
            get => 0 <= index && index < Count ? _tab[index] : throw new IndexOutOfRangeException();
            set
            {
                if (0 <= index && index < Count)
                {
                    _tab[index] = value;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
        }

        private void IncreaseCapacity(int newCapacity = -1)
        {
            if (newCapacity <= 0)
            {
                newCapacity = 2 * Capacity;
            }
            else
            {
                if (newCapacity < Count)
                {
                    throw new ArgumentException("The new capacity cannot be less than the Count");
                }
            }

            T[] newTab = new T[newCapacity];
            Array.Copy(_tab, newTab, Count);
            Capacity = newCapacity;
            _tab = newTab;
        }

        public void Add(T item)
        {
            if (Count >= Capacity)
            {
                IncreaseCapacity();
            }
            _tab[Count] = item;
            Count++;
        }

        public void Clear()
        {
            Count = 0;
        }

        public bool Contains(T item)
        {
            foreach (T element in _tab)
            {
                if (element.Equals(item))
                {
                    return true;
                }
            }
            return false;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            for (int i = 0; i < Count; i++, arrayIndex++)
            {
                array[arrayIndex] = _tab[i];
                // array.SetValue(_tab[i], arrayIndex++);
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new ArrayListEnumerator<T>(this);
        }

        public int IndexOf(T item)
        {
            for (int i = 0; i < Count; i++)
            {
                if (_tab[i].Equals(item))
                {
                    return i;
                }
            }
            return -1;
        }

        public void Insert(int index, T item)
        {
            if (0 > index || index > Count)
            {
                throw new IndexOutOfRangeException();
            }
            if (Count >= Capacity)
            {
                IncreaseCapacity();
            }
            for (int j = Count; j > index; j--)
            {
                _tab[j] = _tab[j-1];
            }
            _tab[index] = item;
            Count++;
        }

        public bool Remove(T item)
        {
            int i = IndexOf(item);
            if (i < 0)
            {
                return false;
            }

            RemoveAt(i);
            return true;
        }

        public void RemoveAt(int index)
        {
            if (0 > index || index >= Count)
            {
                throw new IndexOutOfRangeException();
            }
            for (int j = index + 1; j < Count; j++)
            {
                _tab[j - 1] = _tab[j];
            }
            Count--;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override string ToString()
        {
            return $"({Count}, {Capacity})[{string.Join(", ", _tab)}]";
        }
    }

}
