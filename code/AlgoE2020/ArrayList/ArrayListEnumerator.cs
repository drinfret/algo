﻿using System.Collections;
using System.Collections.Generic;

namespace ArrayList
{
    public class ArrayListEnumerator<T> : IEnumerator<T>
    {
        private readonly ArrayList<T> _list;
        private int _currentIndex;

        internal ArrayListEnumerator(ArrayList<T> list)
        {
            this._list = list;
            _currentIndex = -1;
        }

        public T Current => _list[_currentIndex];

        object IEnumerator.Current => this.Current;

        public void Dispose()
        {
        }

        public bool MoveNext()
        {
            _currentIndex++;
            return _currentIndex < _list.Count;
        }

        public void Reset()
        {
            _currentIndex = -1;
        }
    }
}
