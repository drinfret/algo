using Xunit;
using ArrayList;

namespace XUnitTestArrayList
{
    public class XUnitTestsArrayList
    {
        #region Constructor
        [Fact]
        public void ArrayList_InitialCapacityEqualsDefaultCapacity()
        {
            var expectedCapacity = ArrayList<int>.DefaultCapacity;
            var list = new ArrayList<int>();
            Assert.Equal(expectedCapacity, list.Capacity);
        }

        [Fact]
        public void ArrayList_InitialCapacityEqualsGivenCapacity()
        {
            var expectedCapacity = 5;
            var list = new ArrayList<int>(5);
            Assert.Equal(expectedCapacity, list.Capacity);
        }

        [Fact]
        public void ArrayList_InitialListIsEmpty()
        {
            // ReSharper disable once CollectionNeverUpdated.Local
            var list = new ArrayList<int>();
            Assert.Empty(list);
        }
        #endregion

        #region Add
        [Fact]
        public void ArrayList_Add4ToEmptyList_ListIsSingle()
        {
            var list = new ArrayList<int>(2) {4};
            Assert.Single(list);
        }

        [Fact]
        public void ArrayList_Add3ElementsToEmptyList_ListCountEqual3()
        {
            var list = new ArrayList<int>(2) {4, 7, 2};
            Assert.Equal(3, list.Count);
        }

        [Fact]
        public void ArrayList_Add2ElementsToEmptyListWithCapacity2_CapacityEquals2()
        {
            var list = new ArrayList<int>(2) {4, 7};
            Assert.Equal(2, list.Capacity);
        }

        [Fact]
        public void ArrayList_Add3ElementsToEmptyListWithCapacity2_CapacityEquals4()
        {
            var list = new ArrayList<int>(2) {4, 7, 2};
            Assert.Equal(4, list.Capacity);
        }

        [Fact]
        public void ArrayList_Add4ToEmptyList_FirstElementEquals4()
        {
            var list = new ArrayList<int>(4) {4};
            Assert.Equal(4, list[0]);
        }

        [Fact]
        public void ArrayList_Add4And7And2ToEmptyList_FirstElementEquals4()
        {
            var list = new ArrayList<int>(4) {4, 7, 2};
            Assert.Equal(4, list[0]);
        }

        [Fact]
        public void ArrayList_Add4And7And2ToEmptyList_ThirdElementEquals2()
        {
            var list = new ArrayList<int>(4) {4, 7, 2};
            Assert.Equal(2, list[2]);
        }
        #endregion

        #region Clear
        [Fact]
        public void ArrayList_EmptyList_Clear_ListIsEmpty()
        {
            // ReSharper disable once CollectionNeverUpdated.Local
            var list = new ArrayList<int>(4);
            list.Clear();
            Assert.Empty(list);
        }

        [Fact]
        public void ArrayList_Add3ElementsToEmptyList_Clear_ListIsEmpty()
        {
            var list = new ArrayList<int>(4) {4, 7, 2};
            list.Clear();
            Assert.Empty(list);
        }
        #endregion

        #region Contains
        [Fact]
        public void ArrayList_EmptyList_Contains2IsFalse()
        {
            // ReSharper disable once CollectionNeverUpdated.Local
            var list = new ArrayList<int>(4);
            Assert.DoesNotContain(2, list);
        }

        [Fact]
        public void ArrayList_Add4And7_Contains2IsFalse()
        {
            var list = new ArrayList<int>(4) {4, 7};
            Assert.DoesNotContain(2, list);
        }

        [Fact]
        public void ArrayList_Add4And7And2_Contains2IsTrue()
        {
            var list = new ArrayList<int>(4) {4, 7, 2};
            Assert.Contains(2, list);
        }

        [Fact]
        public void ArrayList_Add2And4And7_Contains2IsTrue()
        {
            var list = new ArrayList<int>(4) {2, 4, 7};
            Assert.Contains(2, list); // Assert.Contains(2, list);
        }
        #endregion
    }
}
