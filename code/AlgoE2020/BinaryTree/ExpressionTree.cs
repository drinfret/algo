﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using StackQueue;

namespace BinaryTree
{
    public class ExpressionTree : BinaryTree<Expression>
    {
        public static Dictionary<string, ExpressionOperator> Operators { get; }
        public static Dictionary<string, ExpressionBracket> LeftBrackets { get; }

        static ExpressionTree()
        {
            Operators = new Dictionary<string, ExpressionOperator>();
            Operators["+"] = new ExpressionOperator("+", (left, right) => left + right);
            Operators["-"] = new ExpressionOperator("-", (left, right) => left - right);
            Operators["*"] = new ExpressionOperator("*", (left, right) => left * right);
            Operators["/"] = new ExpressionOperator("/", (left, right) => left / right);
            
            LeftBrackets = new Dictionary<string, ExpressionBracket>();
            LeftBrackets["("] = new ExpressionBracket("(");
            LeftBrackets["["] = new ExpressionBracket("[");
        }

        public static ExpressionTree FromPostOrder(string postString)
        {
            string[] tokens = postString.Split();
            return FromPostOrder(tokens);
        }
        public static ExpressionTree FromPostOrder(IEnumerable<string> tokens)
        {
            ExpressionTree tree = new ExpressionTree();
            ArrayStack<Node> stack = new ArrayStack<Node>();
            
            foreach (string token in tokens)
            {
                try
                {
                    double element = Convert.ToDouble(token);
                    stack.Push(new Node(new ExpressionValue(element)));
                }
                catch (FormatException)
                {
                    if (Operators.ContainsKey(token))
                    {
                        Node right = stack.Pop();
                        Node left = stack.Pop();
                        Node parent = new Node(Operators[token], null, left, right);
                        left.Parent = parent;
                        right.Parent = parent;
                        stack.Push(parent);
                    }
                    else
                    {
                        throw new ArgumentException($"Invalid token {token}");
                    }
                }
            }

            tree.DummyRoot.Left = stack.Pop();

            return tree;
        }

        public static ExpressionTree FromInOrder(string inString)
        {
            string[] tokens = inString.Split();
            return FromInOrder(tokens);
        }
        
        public static ExpressionTree FromInOrder(IEnumerable<string> tokens)
        {
            ExpressionTree tree = new ExpressionTree();
            ArrayStack<Node> stack = new ArrayStack<Node>();

            foreach (string token in tokens)
            {
                if (LeftBrackets.ContainsKey(token))
                {
                    stack.Push(new Node(LeftBrackets[token]));
                }
                else if (Operators.ContainsKey(token))
                {
                    stack.Push(new Node(Operators[token]));
                }
                else if (token.Equals(")"))
                {
                    Node right = stack.Pop();
                    Node parent = stack.Pop();
                    Node left = stack.Pop();
                    Node bracket = stack.Pop();
                    if (!LeftBrackets.ContainsKey(bracket.Element.ToString()!))
                    {
                        throw new ExpressionTreeException($"Expected a left bracket, got {token} instead");
                    } 
                    left.Parent = parent;
                    right.Parent = parent;
                    parent.Left = left;
                    parent.Right = right;
                    stack.Push(parent);
                }
                else
                {
                    try
                    {
                        double element = Convert.ToDouble(token);
                        stack.Push(new Node(new ExpressionValue(element)));
                    }
                    catch (FormatException)
                    {
                        throw new ArgumentException($"Invalid token {token}");
                    }
                }
            }

            tree.DummyRoot.Left = stack.Pop();

            return tree;
        }

        public override int Count { get; internal set; }

        public override string ToString()
        {
            return ToString(Order.InOrder);
        }

        public string ToString(Order order)
        {
            if (order == Order.InOrder)
            {
                if (DummyRoot.Left is null)
                {
                    return "";
                }

                StringBuilder sb = new StringBuilder();
                ToString(DummyRoot.Left, sb);
                return sb.ToString();
            }

            return string.Join(" ", ToList(order));
        }

        internal void ToString(Node current, StringBuilder sb)
        {
            if (current is null)
            {
                return;
            }

            switch (current.Element)
            {
                case ExpressionValue value:
                    sb.Append(value);
                    break;
                case ExpressionOperator op:
                    sb.Append("(");
                    ToString(current.Left, sb);
                    sb.Append(" ");
                    sb.Append(op);
                    sb.Append(" ");
                    ToString(current.Right, sb);
                    sb.Append(")");
                    break;
            }
        }

        public double Eval()
        {
            if (DummyRoot.Left is null)
            {
                return 0;
            }

            return Eval(DummyRoot.Left);
        }

        internal static double Eval(Node current)
        {
            return current is null
                ? 0
                : current.Element switch
                {
                    ExpressionValue value => value.Value,
                    ExpressionOperator op => op.OperatorFunc(Eval(current.Left), Eval(current.Right)),
                    _ => 0
                };
        }
    }

    public class ExpressionTreeException : Exception
    {
        public ExpressionTreeException(string s) : base(s)
        {
        }
    }


    public abstract class Expression
    {
    }

    public class ExpressionValue : Expression
    {
        public ExpressionValue(double value)
        {
            Value = value;
        }

        public double Value { get; set; }

        public override string ToString()
        {
            return Value.ToString(CultureInfo.CurrentCulture);
        }
    }

    public class ExpressionOperator : Expression
    {
        public ExpressionOperator(string op, Func<double, double, double> operatorFunc)
        {
            Op = op;
            OperatorFunc = operatorFunc;
        }

        public string Op { get; }
        public Func<double, double, double> OperatorFunc { get; set; }

        public override string ToString()
        {
            return Op;
        }
    }

    public class ExpressionBracket : Expression
    {
        public string Bracket { get; }

        public ExpressionBracket(string bracket)
        {
            Bracket = bracket;
        }

        public override string ToString()
        {
            return Bracket;
        }
    }
}