﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BinaryTree
{
    public class TreeDictionary<TKey, TValue> : IDictionary<TKey, TValue> //where TKey : IComparable<TKey>
    {
        private readonly BinarySearchTree<KeyValuePair<TKey, TValue>> _tree;

        public TreeDictionary(Func<KeyValuePair<TKey, TValue>, KeyValuePair<TKey, TValue>, int> comparator = null)
        {
            comparator ??= (x, y) => Comparer<TKey>.Default.Compare(x.Key, y.Key);

            _tree = new BinarySearchTree<KeyValuePair<TKey, TValue>>(comparator);
        }


        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            foreach (var pair in _tree)
            {
                yield return pair;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            if (item.Key is null)
            {
                throw new ArgumentNullException();
            }

            _tree.Add(item);
        }

        public void Clear()
        {
            _tree.Clear();
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            if (item.Key is null)
            {
                throw new ArgumentNullException();
            }

            return _tree.Contains(item);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            foreach (var pair in _tree)
            {
                array[arrayIndex] = pair;
                arrayIndex++;
            }
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            if (item.Key is null)
            {
                throw new ArgumentNullException();
            }

            return _tree.Remove(item);
        }

        public int Count => _tree.Count;
        public bool IsReadOnly => _tree.IsReadOnly;

        public void Add(TKey key, TValue value)
        {
            if (key is null)
            {
                throw new ArgumentNullException();
            }

            _tree.Add(new KeyValuePair<TKey, TValue>(key, value));
        }

        public bool ContainsKey(TKey key)
        {
            if (key is null)
            {
                throw new ArgumentNullException();
            }

            return _tree.Contains(new KeyValuePair<TKey, TValue>(key, default));
        }

        public bool Remove(TKey key)
        {
            if (key is null)
            {
                throw new ArgumentNullException();
            }

            return _tree.Remove(new KeyValuePair<TKey, TValue>(key, default));
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            if (key is null)
            {
                throw new ArgumentNullException();
            }

            var node = _tree.FindNode(new KeyValuePair<TKey, TValue>(key, default));
            value = node is null ? default : node.Element.Value;
            return !(node is null);
        }

        public TValue this[TKey key]
        {
            get
            {
                if (key is null)
                {
                    throw new ArgumentNullException();
                }

                var node = _tree.FindNode(new KeyValuePair<TKey, TValue>(key, default));
                return node is null ? throw new KeyNotFoundException() : node.Element.Value;
            }
            set
            {
                if (key is null)
                {
                    throw new ArgumentNullException();
                }

                var node = _tree.FindNode(new KeyValuePair<TKey, TValue>(key, default));
                var keyValuePair = new KeyValuePair<TKey, TValue>(key, value);
                if (node is null)
                {
                    _tree.Add(keyValuePair);
                }
                else
                {
                    node.Element = keyValuePair;
                }
            }
        }

        public ICollection<TKey> Keys
        {
            get { return _tree.Select(pair => pair.Key).ToArray(); }
        }

        public ICollection<TValue> Values
        {
            get
            {
                // var list = new ArrayList<TValue>();
                // foreach (var pair in _tree)
                // {
                //     list.Add(pair.Pair.Value);
                // }
                //
                // return list;
                return _tree.Select(pair => pair.Value).ToArray();
            }
        }
    }
}