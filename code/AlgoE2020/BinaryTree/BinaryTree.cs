﻿using System;
using System.Collections;
using System.Collections.Generic;
using ArrayList;
using StackQueue;

namespace BinaryTree
{
    public abstract class BinaryTree<T> : IEnumerable<T>
    {
        internal class Node
        {
            public Node(T element, Node parent = null, Node left = null, Node right = null)
            {
                Element = element;
                Parent = parent;
                Left = left;
                Right = right;
            }

            public T Element { get; internal set; }
            public Node Parent { get; internal set; }
            public Node Left { get; internal set; }
            public Node Right { get; internal set; }
        }

        internal Node DummyRoot;
        public abstract int Count { get; internal set; }

        protected BinaryTree()
        {
            DummyRoot = new Node(default);
        }

        public virtual IEnumerator<T> GetEnumerator()
        {
            // building a list, then returning an enumerator on it works, but is less efficient since we will end up
            // going through the elements twice: once to build the list, then another time in the actual enumerator
            //return ToList().GetEnumerator();

            // InOrder traversal of the tree
            // simulating a recursive algorithm using an explicit stack instead of relying on the call stack
            // using yield return and yield break to create an Enumerator<T> implicitly
            // this is a kind of item generator
            ArrayStack<Node> stack = new ArrayStack<Node>();
            Node current = DummyRoot.Left;

            // will stop when current is null and the stack is empty
            while (true)
            {
                // current is null when we need to "return" from a "recursive call"
                if (current is null)
                {
                    // if the stack is empty, then there's nothing to pop, so we finish the recursion
                    if (stack.Count == 0)
                    {
                        yield break;
                    }

                    // pop the node we need to go back to
                    current = stack.Pop();
                    // we go back to processing the node after completing the left subtree
                    // we yield the current element
                    yield return current.Element;
                    // we move to the right subtree
                    // we don't push anything on the stack here, because after processing the right subtree, 
                    // there's nothing left to do (there's nothing to execute after the right recursive call returns)
                    current = current.Right;
                }
                else
                {
                    // when current is not null, we push current to remember where to go back, and we move to the left
                    stack.Push(current);
                    current = current.Left;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Clear()
        {
            //TODO: is this enough for the garbage collector to do its job?
            DummyRoot = null;
            Count = 0;
        }

        public virtual bool Contains(T item)
        {
            // sequential search to find the item
            foreach (T element in this)
            {
                if (element.Equals(item))
                {
                    return true;
                }
            }

            return false;
        }

        public virtual void CopyTo(T[] array, int arrayIndex)
        {
            foreach (T element in this)
            {
                array[arrayIndex] = element;
                arrayIndex++;
            }
        }
        

        public virtual IList<T> ToList(Order order = Order.InOrder)
        {
            ArrayList<T> list = new ArrayList<T>();
            if (DummyRoot.Left is null) return list;
            switch (order)
            {
                case Order.PreOrder:
                    ToPreOrderList(DummyRoot.Left, list);
                    break;
                case Order.InOrder:
                    ToInOrderList(DummyRoot.Left, list);
                    break;
                case Order.PostOrder:
                    ToPostOrderList(DummyRoot.Left, list);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(order), order, null);
            }

            return list;
        }

        internal virtual void ToInOrderList(Node node, IList<T> list)
        {
            if (node is null)
            {
                return;
            }

            ToInOrderList(node.Left, list);
            list.Add(node.Element);
            ToInOrderList(node.Right, list);
        }

        internal virtual void ToPreOrderList(Node node, IList<T> list)
        {
            if (node is null)
            {
                return;
            }

            list.Add(node.Element);
            ToPreOrderList(node.Left, list);
            ToPreOrderList(node.Right, list);
        }

        internal virtual void ToPostOrderList(Node node, IList<T> list)
        {
            if (node is null)
            {
                return;
            }

            ToPostOrderList(node.Left, list);
            ToPostOrderList(node.Right, list);
            list.Add(node.Element);
        }


        public virtual void ForEach(Action<T> processElement, Order order = Order.InOrder)
        {
            if (DummyRoot.Left is null) return;
            switch (order)
            {
                case Order.PreOrder:
                    PreOrderTraversal(DummyRoot.Left, processElement);
                    break;
                case Order.InOrder:
                    InOrderTraversal(DummyRoot.Left, processElement);
                    break;
                case Order.PostOrder:
                    PostOrderTraversal(DummyRoot.Left, processElement);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(order), order, null);
            }
        }

        internal virtual void InOrderTraversal(Node node, Action<T> processElement)
        {
            if (node is null)
            {
                return;
            }

            InOrderTraversal(node.Left, processElement);
            processElement(node.Element);
            InOrderTraversal(node.Right, processElement);
        }

        internal virtual void PreOrderTraversal(Node node, Action<T> processElement)
        {
            if (node is null)
            {
                return;
            }

            processElement(node.Element);
            PreOrderTraversal(node.Left, processElement);
            PreOrderTraversal(node.Right, processElement);
        }

        internal virtual void PostOrderTraversal(Node node, Action<T> processElement)
        {
            if (node is null)
            {
                return;
            }

            PostOrderTraversal(node.Left, processElement);
            PostOrderTraversal(node.Right, processElement);
            processElement(node.Element);
        }
    }

    public enum Order
    {
        PreOrder,
        InOrder,
        PostOrder
    }
}