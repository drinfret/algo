﻿using System;
using System.Collections.Generic;

namespace BinaryTree
{
    public class BinarySearchTree<T> : BinaryTree<T>, ICollection<T> //where T : IComparable<T>
    {
        public Func<T, T, int> Comparator { get; }

        public BinarySearchTree(Func<T, T, int> comparator = null)
        {
            Comparator = comparator ?? Comparer<T>.Default.Compare;

            Count = 0;
        }

        public void Add(T item)
        {
            if (item == null) throw new ArgumentNullException(nameof(item));

            Node parent = DummyRoot;
            Node current = DummyRoot.Left; // real root
            bool fromLeft = true; // where are we coming from; true if parent.Left points to current
            // false otherwise (if parent.Right points to current)

            while (current != null)
            {
                parent = current;
                if (Comparator(item, current.Element) < 0)
                {
                    current = current.Left;
                    fromLeft = true;
                }
                else
                {
                    current = current.Right;
                    fromLeft = false;
                }
            }

            if (fromLeft)
            {
                parent.Left = new Node(item, parent);
            }
            else
            {
                parent.Right = new Node(item, parent);
            }

            Count++;
        }

        public override bool Contains(T item)
        {
            // can do better than sequential search here because the elements are kept in order
            // can do a (kind of) binary search
            return FindNode(item) != null;
        }

        public bool Remove(T item)
        {
            if (item == null) throw new ArgumentNullException(nameof(item));

            Node current = FindNode(item);
            if (current == null)
            {
                return false;
            }

            if (current.Left == null)
            {
                if (current.Right == null) // if both Left and Right are null, then current is a leaf
                {
                    if (current.Parent.Left == current)
                    {
                        current.Parent.Left = null;
                    }
                    else
                    {
                        current.Parent.Right = null;
                    }
                }
                else // if Left is null and Right is NOT null, move Right node to replace current
                {
                    if (current.Parent.Left == current)
                    {
                        current.Parent.Left = current.Right;
                    }
                    else
                    {
                        current.Parent.Right = current.Right;
                    }

                    current.Right.Parent = current.Parent;
                    current.Right = null;
                }

                current.Parent = null;
            }
            else
            {
                if (current.Right == null) // if Left is NOT null and Right is null, move Left node to replace current
                {
                    if (current.Parent.Left == current)
                    {
                        current.Parent.Left = current.Left;
                    }
                    else
                    {
                        current.Parent.Right = current.Left;
                    }
                }
                else // if Left is NOT null and Right is NOT null, current has 2 children nodes
                {
                    var maxFromLeft = RemoveMaxFromLeft(current);

                    // replace current element by max element
                    current.Element = maxFromLeft;
                }
            }

            Count--;
            return true;
        }

        private static T RemoveMaxFromLeft(Node current)
        {
            // find max element from the left
            // max element is the right-most element
            Node maxFromLeft = current.Left;
            while (maxFromLeft.Right != null)
            {
                maxFromLeft = maxFromLeft.Right;
            }

            // if the max element in the left is directly to the left of current
            // set the left of current (or left of maxFromLeft.Parent) to the left of maxFromLeft
            if (maxFromLeft.Parent == current)
            {
                current.Left = maxFromLeft.Left;
            }
            else // otherwise, it's to the right of the parent
            {
                maxFromLeft.Parent.Right = maxFromLeft.Left;
            }

            if (maxFromLeft.Left != null)
            {
                maxFromLeft.Left.Parent = maxFromLeft.Parent;
            }

            maxFromLeft.Parent = null;
            maxFromLeft.Left = null;

            return maxFromLeft.Element;
        }

        internal Node FindNode(T item)
        {
            if (item == null) throw new ArgumentNullException(nameof(item));

            Node current = DummyRoot.Left;
            while (current != null)
            {
                int comp = Comparator(item, current.Element);
                if (comp == 0)
                {
                    return current;
                }

                current = comp < 0 ? current.Left : current.Right;
            }

            return null;
        }

        public T FindElement(T item)
        {
            Node node = FindNode(item);
            return node is null ? default : node.Element;
        }

        public sealed override int Count { get; internal set; }
        public bool IsReadOnly => false;
    }
}