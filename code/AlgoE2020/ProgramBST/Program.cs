﻿using System;
using ArrayList;
using BinaryTree;

namespace ProgramBST
{
    class Program
    {
        static void Main()
        {
            BinarySearchTree<int> bst = new BinarySearchTree<int>()//((x, y) => y.CompareTo(x)))
                {9, 8, 3, 4, 12, 6, 2, 9, 1, 7, 4, 6, 9, 11, 17, 10, 22, 5, 7, 12, -4, -3, 12};
            Console.WriteLine(bst.Count);

            Console.WriteLine(string.Join(" . ", bst));
            Console.WriteLine("Remove 6: {0}", bst.Remove(6));
            Console.WriteLine(string.Join(" . ", bst));
            Console.WriteLine("Remove 6: {0}", bst.Remove(6));
            Console.WriteLine(string.Join(" . ", bst));
            Console.WriteLine("Remove 6: {0}", bst.Remove(6));
            Console.WriteLine(string.Join(" . ", bst));
            Console.WriteLine("Remove 12: {0}", bst.Remove(12));
            Console.WriteLine(string.Join(" . ", bst));
            Console.WriteLine("Remove 22: {0}", bst.Remove(22));
            Console.WriteLine(string.Join(" . ", bst));
            Console.WriteLine("Remove 9: {0}", bst.Remove(9));
            Console.WriteLine(string.Join(" . ", bst));
            Console.WriteLine("Remove 9: {0}", bst.Remove(9));
            Console.WriteLine(string.Join(" . ", bst));
            Console.WriteLine("Remove 9: {0}", bst.Remove(9));
            Console.WriteLine(string.Join(" . ", bst));
            Console.WriteLine("Remove 9: {0}", bst.Remove(9));
            Console.WriteLine(string.Join(" . ", bst));
            Console.WriteLine("Remove -4: {0}", bst.Remove(-4));
            Console.WriteLine(string.Join(" . ", bst));

            Console.WriteLine("InOrder: Enumerator");
            foreach (int x in bst)
            {
                Console.WriteLine(x);
            }

            Console.WriteLine("InOrder: ForEach");
            // bst.ForEach(Console.WriteLine);
            bst.ForEach(x => Console.Write("{0}, ", x));
            Console.WriteLine("\nInOrder: Enumerator + string.Join");
            Console.WriteLine(string.Join(", ", bst));
            Console.WriteLine("InOrder: ToList");
            Console.WriteLine(string.Join(", ", bst.ToList()));

            Console.WriteLine("InOrder: ForEach to list");
            var list = new ArrayList<int>();
            bst.ForEach(x => list.Add(x));
            Console.WriteLine(string.Join(", ", list));

            Console.WriteLine("PreOrder: ForEach");
            bst.ForEach(x => Console.Write("{0}, ", x), Order.PreOrder);
            Console.WriteLine("\nPreOrder: ToList");
            Console.WriteLine(string.Join(", ", bst.ToList(Order.PreOrder)));

            Console.WriteLine("PreOrder: ForEach to list");
            list.Clear();
            bst.ForEach(x => list.Add(x), Order.PreOrder);
            Console.WriteLine(string.Join(", ", list));

            Console.WriteLine("PostOrder: ForEach");
            bst.ForEach(x => Console.Write("{0}, ", x), Order.PostOrder);
            Console.WriteLine("\nPostOrder: ToList");
            Console.WriteLine(string.Join(", ", bst.ToList(Order.PostOrder)));

            Console.WriteLine("PostOrder: ForEach to list");
            list.Clear();
            bst.ForEach(x => list.Add(x), Order.PostOrder);
            Console.WriteLine(string.Join(", ", list));
        }
    }
}