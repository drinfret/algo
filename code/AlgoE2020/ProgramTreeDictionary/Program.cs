﻿using System;
using System.Collections.Generic;
using BinaryTree;

namespace ProgramTreeDictionary
{
    class Program
    {
        static void Main()
        {
            var dict = new TreeDictionary<string, int>();
            dict["abc"] = 5;
            dict["def"] = 10;
            dict["qwe"] = 7;
            foreach (var pair in dict)
            {
                Console.WriteLine(pair);    
            }
            Console.WriteLine("Contains key \"abc\": {0}", dict.ContainsKey("abc"));
            Console.WriteLine("Contains key \"xyz\": {0}", dict.ContainsKey("xyz"));
            Console.WriteLine("Get key \"abc\": {0}", dict["abc"]);
            try
            {
                Console.WriteLine("Get key \"xyz\": {0}", dict["xyz"]);
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("Cannot get key \"xyz\"");
            }

            int value;
            dict.TryGetValue("abc", out value);
            Console.WriteLine("TryGetValue \"abc\": {0}", value);
            dict.TryGetValue("xyz", out value);
            Console.WriteLine("TryGetValue \"xyz\": {0}", value);
            Console.WriteLine($"Keys {string.Join(", ", dict.Keys)}");
            Console.WriteLine($"Values {string.Join(", ", dict.Values)}");
        }
    }
}