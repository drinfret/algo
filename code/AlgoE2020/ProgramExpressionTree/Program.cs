﻿using System;
using BinaryTree;

namespace ProgramExpressionTree
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                Console.Error.WriteLine("usage: order expression");
                return;
            }

            ExpressionTree tree;
            if (args[0].Equals("post"))
            {
                tree = ExpressionTree.FromPostOrder(args[1]);
            } else if (args[0].Equals("in"))
            {
                tree = ExpressionTree.FromInOrder(args[1]);
            }
            else
            {
                Console.Error.WriteLine("usage: order expression");
                return;
            }

            Console.WriteLine(tree);
            
            Console.WriteLine(tree.Eval());
            
            Console.WriteLine(string.Join(" ", tree));

        }
    }
}